package com.positiveminds.smartcity.controller.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.server.FavaDiagnosticsEntity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.SmartCityApplication;
import com.positiveminds.smartcity.controller.OperationManager;
import com.positiveminds.smartcity.controller.adapter.SearchListAdapter;
import com.positiveminds.smartcity.model.Listener.DBOperationListener;
import com.positiveminds.smartcity.model.Listener.PlaceSearchOperationListener;
import com.positiveminds.smartcity.model.Listener.RecyclerItemClickListener;
import com.positiveminds.smartcity.model.constant.AppConstant;
import com.positiveminds.smartcity.model.db.FavouritePlacesContract;
import com.positiveminds.smartcity.model.entity.Person;
import com.positiveminds.smartcity.model.entity.PlaceSearchInfo;
import com.positiveminds.smartcity.model.server.Url;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RajeevTomar on 9/9/2015.
 */
public class VenueListScreen extends BaseActivity {

    private CollapsingToolbarLayout collapsingToolbarLayout;
    private List<PlaceSearchInfo> mSearchInfoList;
    private GoogleMap mGoogleMap;
    private ClusterManager<Person> mClusterManager;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private boolean showingFirstTime = false;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue_list);
        initAdView(R.id.venue_list_adView);
        initView();
        init();
    }

    private void initView() {
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_venue_list);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
    }

    private void init() {
        adjustHeaderImageDimen();
        mProgressBar = findViewById(R.id.bar_venue_list_activity);
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
        collapsingToolbarLayout.setContentScrimColor(getResources().getColor(R.color.theme_color_transparent));
        collapsingToolbarLayout.setStatusBarScrimColor(getResources().getColor(R.color.theme_color));
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        setUpMapIfNeeded();
        if (getIntent() != null) {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                String placeName = bundle.getString(AppConstant.BUNDLE_PLACE_TYPE);
                int venueSaveType = bundle.getInt(AppConstant.BUNDLE_VENUE_SAVE_TYPE);
                if (!TextUtils.isEmpty(placeName)) {
                    collapsingToolbarLayout.setTitle(placeName);
                    searchUserQuery(placeName);
                } else if (venueSaveType > 0) {
                    collapsingToolbarLayout.setTitle("Favourite Venues");
                    getVenuePlacesFromDB(venueSaveType);
                }
            }
        }
    }

    private void setUpMapIfNeeded() {
        if (mGoogleMap != null) {
            return;
        }
        mGoogleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_venue_list)).getMap();
    }

    private void searchUserQuery(final String query) {
        try {
            LatLng currentLocation = SmartCityApplication.getAppInstance().getCurrentLocation();
            if (currentLocation != null) {
//                final String encodeQuery = URLEncoder.encode(query, "UTF-8");
//                String url = Url.getUserQueryUrl(query, currentLocation);
                showProgress();
                String url = Url.getFoursquarePlaceSearchUrl(query, currentLocation, null);
                OperationManager.getInstance().getSearchPlaceList(url, new PlaceSearchOperationListener() {
                    @Override
                    public void onSuccessPlaceSearch(List<PlaceSearchInfo> searchInfoList) {
                        if (searchInfoList != null && searchInfoList.size() > 0) {
                            hideProgress();
                            mSearchInfoList = searchInfoList;
                            initRecyclerVIew();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    showInterstitialAdd();
                                }
                            },2000);
                        }
                    }

                    @Override
                    public void onFailurePlaceSearch(com.positiveminds.smartcity.model.entity.Error error) {
                        hideProgress();
                        showSnackBar(error.getMessage());
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                finish();
                            }
                        }, 1000);

                    }
                });
            } else {
                showSnackBar("Location is off!");
                openLocationDialog();
            }
        } catch (Exception exception) {
            if (exception != null) {
                showSnackBar(exception.getMessage());
            }
        }

    }

    private void initRecyclerVIew() {

        if (mSearchInfoList != null && mSearchInfoList.size() > 0) {
            SearchListAdapter listAdapter = new SearchListAdapter(mSearchInfoList);
            recyclerView.setAdapter(listAdapter);
            initMapView();
        }
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(VenueListScreen.this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        onClickItem(position);
                    }
                })
        );
    }


    private void initMapView() {
        if (mGoogleMap != null) {
            setUpCluster(mSearchInfoList.get(0).getLocation());
        }
    }

    private void setUpCluster(LatLng location) {
        // Position the map.
        if (location != null) {
            float zoom = 11;
            if (mSearchInfoList.size() <= 6)
                zoom = 14;
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, zoom));
        }

        // Initialize the manager with the context and the map.
        // (Activity extends context, so we can pass 'this' in the constructor.)
        mClusterManager = new ClusterManager<Person>(this, mGoogleMap);
        mClusterManager.setRenderer(new PersonRenderer());

        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
        mGoogleMap.setOnCameraChangeListener(mClusterManager);
        mGoogleMap.setOnMarkerClickListener(mClusterManager);
        mGoogleMap.setOnInfoWindowClickListener(mClusterManager);
        mGoogleMap.setInfoWindowAdapter(new MyCustomAdapterForItems());
        mClusterManager.setOnClusterItemInfoWindowClickListener(new ClusterManager.OnClusterItemInfoWindowClickListener<Person>() {
            @Override
            public void onClusterItemInfoWindowClick(Person person) {
                onClickItem(person.getItemPosition());
            }
        });
        // Add cluster items (markers) to the cluster manager.
        addItems();
        mClusterManager.cluster();
    }

    private void onClickItem(int position) {
        if (isInternetConnected()) {
            PlaceSearchInfo searchInfo = mSearchInfoList.get(position);
            if (searchInfo != null) {
                String placeId = searchInfo.getPlaceId();
                if (placeId != null) {
                    Intent intent = new Intent(VenueListScreen.this, VenueDetailActivity.class);
                    SmartCityApplication.getAppInstance().setDetailsInfo(searchInfo);
                    intent.putExtra(AppConstant.BUNDLE_PLACE_ID, placeId);
                    intent.putExtra(AppConstant.BUNDLE_IMAGE_URL, searchInfo.getIconUrl());
                    startActivity(intent);
                }
            }
        } else
            showNoInternetDialog();

    }


    private void addItems() {
        for (int i = 0; i < mSearchInfoList.size(); i++) {
            PlaceSearchInfo info = mSearchInfoList.get(i);
            LatLng location = info.getLocation();
            if (location != null) {
                Person offsetItem = new Person(location, info.getName(), info.getIconUrl(), i, info.getAddress());
                mClusterManager.addItem(offsetItem);
            }
        }
    }


    private void adjustHeaderImageDimen() {
        Display display = getWindowManager().getDefaultDisplay();
        int height = (display.getHeight() * 55) / 100;// ((display.getHeight()*30)/100)
        SupportMapFragment mMapFragment = (SupportMapFragment) (getSupportFragmentManager()
                .findFragmentById(R.id.map_venue_list));
        ViewGroup.LayoutParams params = mMapFragment.getView().getLayoutParams();
        params.height = height;
        mMapFragment.getView().setLayoutParams(params);
    }

    private class PersonRenderer extends DefaultClusterRenderer<Person> {
        private final IconGenerator mIconGenerator = new IconGenerator(getApplicationContext());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());

        public PersonRenderer() {
            super(getApplicationContext(), mGoogleMap, mClusterManager);
        }

        @Override
        protected void onBeforeClusterItemRendered(Person person, MarkerOptions markerOptions) {
            super.onBeforeClusterItemRendered(person, markerOptions);
            String title = String.format("%s\n%s", person.name, person.getAddress());
            markerOptions.title(title).snippet(person.getProfilePhoto());

        }

        @Override
        protected void onBeforeClusterRendered(Cluster<Person> cluster, MarkerOptions markerOptions) {
            super.onBeforeClusterRendered(cluster, markerOptions);
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            return cluster.getSize() > 1;
        }
    }

    public class MyCustomAdapterForItems implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;


        MyCustomAdapterForItems() {
            myContentsView = getLayoutInflater().inflate(
                    R.layout.info_window, null);
        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(final Marker marker) {

            ImageView imageView = (ImageView) myContentsView.findViewById(R.id.iv_marker);
            String iconUrl = marker.getSnippet();
            if (!TextUtils.isEmpty(iconUrl)) {
                if (!showingFirstTime) {
                    imageLoader.displayImage(iconUrl, imageView, options, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String s, View view) {

                        }

                        @Override
                        public void onLoadingFailed(String s, View view, FailReason failReason) {

                        }

                        @Override
                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                            showingFirstTime = true;
                            marker.showInfoWindow();
                        }

                        @Override
                        public void onLoadingCancelled(String s, View view) {

                        }
                    });
                } else {
                    showingFirstTime = false;
                    imageLoader.displayImage(iconUrl, imageView, options);
                }
            }
            TextView tvTitle = ((TextView) myContentsView
                    .findViewById(R.id.tv_marker_title));
            String title = marker.getTitle();
            if (!TextUtils.isEmpty(title))
                tvTitle.setText(title);
            else
                return null;
            return myContentsView;
        }
    }


    private void getVenuePlacesFromDB(int venueType) {
        showProgress();
        OperationManager.getInstance().getVenuePlacesFromDB(new DBOperationListener() {
            @Override
            public void onSuccessInsertDB() {

            }

            @Override
            public void onSuccessUpdateDB() {

            }

            @Override
            public void onSuccessDeleteDB() {

            }

            @Override
            public void onSuccessReadDB(Cursor cursor) {

                if (cursor != null) {
                    ArrayList<PlaceSearchInfo> searchInfos = new ArrayList<PlaceSearchInfo>();
                    if (cursor.moveToFirst()) {
                        do {
                            PlaceSearchInfo searchInfo = new PlaceSearchInfo();
                            String placeName = cursor.getString(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_NAME));
                            searchInfo.setName(placeName);
                            String placeId = cursor.getString(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_ID));
                            searchInfo.setPlaceId(placeId);
                            String placeAddress = cursor.getString(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_ADDRESS));
                            searchInfo.setAddress(placeAddress);
                            String distance = cursor.getString(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_DISTANCE));
                            searchInfo.setDistanceStr(distance);
                            int vote = cursor.getInt(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_VOTE));
                            searchInfo.setVote(vote);
                            String rating = cursor.getString(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_RATING));
                            searchInfo.setRating(rating);
                            String category = cursor.getString(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_CATEGORY));
                            searchInfo.setCategory(category);
                            String url = cursor.getString(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_URL));
                            searchInfo.setIconUrl(url);
                            int venueSaveType = cursor.getInt(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_VENUE_TYPE));
                            searchInfo.setVenueSaveType(venueSaveType);
                            String latitude = cursor.getString(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_LAT));
                            String longitude = cursor.getString(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_LONG));
                            if (!TextUtils.isEmpty(latitude) && !TextUtils.isEmpty(longitude)) {
                                double lat = Double.valueOf(latitude);
                                double longi = Double.valueOf(longitude);
                                LatLng locationVenue = new LatLng(lat, longi);
                                searchInfo.setLocation(locationVenue);
                            }
                            searchInfos.add(searchInfo);
                        }
                        while (cursor.moveToNext());
                    } else {
                        hideProgress();
                    }
                    cursor.close();
                    if (searchInfos.size() > 0) {

                        hideProgress();
                        mSearchInfoList = searchInfos;
                        initRecyclerVIew();

                    } else {
                        hideProgress();
                        showNoResultFoundAlert("No Favourite venue added.", "Would you like to go back.");
                    }

                }
            }

            @Override
            public void onFailureDB(String errorMessage) {
                hideProgress();
                showSnackBar(errorMessage);
            }
        }, venueType);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        showInterstitialAdd();
    }
}
