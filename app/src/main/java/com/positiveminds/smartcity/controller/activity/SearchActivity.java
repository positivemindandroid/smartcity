package com.positiveminds.smartcity.controller.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.SmartCityApplication;
import com.positiveminds.smartcity.controller.OperationManager;
import com.positiveminds.smartcity.controller.adapter.HomeAdapter;
import com.positiveminds.smartcity.controller.adapter.SearchQueryAdapter;
import com.positiveminds.smartcity.model.Listener.PlaceSearchOperationListener;
import com.positiveminds.smartcity.model.Listener.RecyclerItemClickListener;
import com.positiveminds.smartcity.model.Listener.SuggestionOperationListener;
import com.positiveminds.smartcity.model.constant.AppConstant;
import com.positiveminds.smartcity.model.entity.*;
import com.positiveminds.smartcity.model.entity.Error;
import com.positiveminds.smartcity.model.server.Url;
import com.positiveminds.smartcity.widget.SimpleDividerItemDecoration;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Created by RajeevTomar on 8/31/2015.
 */
public class SearchActivity extends BaseActivity {

    private EditText mSearchEditText;
    private RecyclerView mRecyclerView;
    private List<String> mPlaceTypeList;
    private List<String> mPlaceSuggestionList;
    private List<String> mFilteredModelList;
    private SearchQueryAdapter mSearchQueryAdapter;
    private boolean mUserSearch = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initAdView(R.id.search_adView);
        initView();
        //blurImage();
        initRecyclerView();
    }

    private void initView() {
        mSearchEditText = (EditText) findViewById(R.id.et_search_query);
        mRecyclerView = (RecyclerView) findViewById(R.id.search_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));
        mSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(final Editable s) {
                filter(s.toString());
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        handleAutoSuggestList(s.toString());
//                    }
//                }, 1000);
//
            }
        });
        mSearchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String query = mSearchEditText.getText().toString();
                    if(!TextUtils.isEmpty(query))
                        openVenueListScreen(query);
                    return true;
                }
                return false;
            }
        });

    }

    private void blurImage() {
        ImageView imageView = (ImageView) findViewById(R.id.iv_search_blurr);
        BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
        Bitmap bitmap = drawable.getBitmap();
        Bitmap blurred = blurRenderScript(bitmap, 25);//second parametre is radius
        imageView.setImageBitmap(blurred);

    }


    private void initRecyclerView() {
        String[] placeTypeArr = getResources().getStringArray(R.array.suggested_place_array);
        mPlaceSuggestionList = Arrays.asList(placeTypeArr);
        String[] filterPlaceArr = getResources().getStringArray(R.array.place_type_array);
        mPlaceTypeList = Arrays.asList(filterPlaceArr);
        mFilteredModelList = mPlaceSuggestionList;
        mSearchQueryAdapter = new SearchQueryAdapter(mPlaceSuggestionList, SearchActivity.this);
        mRecyclerView.setAdapter(mSearchQueryAdapter);
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(SearchActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        onClickSearchQuery(position);
                    }
                })
        );
    }

    private void onClickSearchQuery(int position) {
        String query = mFilteredModelList.get(position);
        openVenueListScreen(query);
    }

    private void openVenueListScreen(String query) {
        Intent intent = new Intent(SearchActivity.this, VenueListScreen.class);
        intent.putExtra(AppConstant.BUNDLE_PLACE_TYPE, query);
        startActivity(intent);
    }


    private void filter(String query) {
        query = query.toLowerCase();
        if (query.equals("")) {
            mFilteredModelList = mPlaceSuggestionList;
        } else {
            mFilteredModelList = new ArrayList<>();
            for (String searchType : mPlaceTypeList) {
                if (searchType.toLowerCase(Locale.getDefault()).contains(query)) {
                    mFilteredModelList.add(searchType);
                    mUserSearch = false;
                }
            }
            if (mFilteredModelList.size() == 0) {
                mUserSearch = true;
                mFilteredModelList.add(query);
            }
        }
        mSearchQueryAdapter.animateTo(mFilteredModelList);
        mRecyclerView.scrollToPosition(0);
    }


    private void handleAutoSuggestList(final String query) {
        try {
            String userQuery = query.toLowerCase();
            //userQuery = URLEncoder.encode(userQuery, "UTF-8");
            if (userQuery.equals("")) {
                mFilteredModelList = mPlaceSuggestionList;
            }
            if (!TextUtils.isEmpty(userQuery) && userQuery.length() >= 4) {

                LatLng currentLocation = SmartCityApplication.getAppInstance().getCurrentLocation();
                OperationManager.getInstance().getAutoSuggestionList(userQuery, currentLocation, new SuggestionOperationListener() {
                    @Override
                    public void onSuccessfullGetSuggestion(List<AutoSuggestion> autoSuggestions) {
                        mFilteredModelList = new ArrayList<>();
                        if (autoSuggestions != null && autoSuggestions.size() > 0) {
                            for (AutoSuggestion info : autoSuggestions) {
                                mFilteredModelList.add(info.getName());
                            }
                        } else {
                            mFilteredModelList.add(query);
                        }
                        mSearchQueryAdapter.animateTo(mFilteredModelList);
                        mRecyclerView.scrollToPosition(0);
                    }

                    @Override
                    public void onFailureGetSuggestion(Error error) {

                    }
                });
            }
        } catch (Exception exception) {
            if (exception != null) {
                showSnackBar(exception.getMessage());
            }
        }
    }

    @SuppressLint("NewApi")
    private Bitmap blurRenderScript(Bitmap smallBitmap, int radius) {

        try {
            smallBitmap = RGB565toARGB888(smallBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }


        Bitmap bitmap = Bitmap.createBitmap(
                smallBitmap.getWidth(), smallBitmap.getHeight(),
                Bitmap.Config.ARGB_8888);

        RenderScript renderScript = RenderScript.create(SearchActivity.this);

        Allocation blurInput = Allocation.createFromBitmap(renderScript, smallBitmap);
        Allocation blurOutput = Allocation.createFromBitmap(renderScript, bitmap);

        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript,
                Element.U8_4(renderScript));
        blur.setInput(blurInput);
        blur.setRadius(radius); // radius must be 0 < r <= 25
        blur.forEach(blurOutput);

        blurOutput.copyTo(bitmap);
        renderScript.destroy();

        return bitmap;

    }

    private Bitmap RGB565toARGB888(Bitmap img) throws Exception {
        int numPixels = img.getWidth() * img.getHeight();
        int[] pixels = new int[numPixels];

        //Get JPEG pixels.  Each int is the color values for one pixel.
        img.getPixels(pixels, 0, img.getWidth(), 0, 0, img.getWidth(), img.getHeight());

        //Create a Bitmap of the appropriate format.
        Bitmap result = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);

        //Set RGB pixels.
        result.setPixels(pixels, 0, result.getWidth(), 0, 0, result.getWidth(), result.getHeight());
        return result;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        showInterstitialAdd();
    }
}
