package com.positiveminds.smartcity.controller.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.SmartCityApplication;
import com.positiveminds.smartcity.controller.DBManager;
import com.positiveminds.smartcity.controller.OperationManager;
import com.positiveminds.smartcity.controller.fragment.VenueDetailFragment;
import com.positiveminds.smartcity.model.Listener.PlaceDetailsOperationListener;
import com.positiveminds.smartcity.model.constant.AppConstant;
import com.positiveminds.smartcity.model.db.FavouritePlacesContract;
import com.positiveminds.smartcity.model.entity.PlaceDetailsInfo;
import com.positiveminds.smartcity.model.entity.PlaceSearchInfo;
import com.positiveminds.smartcity.model.server.Url;
import com.positiveminds.smartcity.widget.SweetAlertDialog;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by RajeevTomar on 9/4/2015.
 */
public class VenueDetailActivity extends BaseActivity implements PlaceDetailsOperationListener, View.OnClickListener, VenueDetailFragment.VenueDetailFragmentListener {

    private CollapsingToolbarLayout collapsingToolbarLayout;
    private ImageView mHeaderImageView;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private String mPlaceId;
    private VenueDetailFragment mVenueDetailFragment;
    private View parentView;
    private String mCardImagePath;
    private PlaceDetailsInfo mDetailsInfo;
    private String mDistance;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue_detail);
        initAdView(R.id.venue_detail_adView);
        initView();
        init();
        initDetailFragment(null);
        initVenueInfo();
        getVenueDetail();
    }

    private void initView() {
        parentView = findViewById(R.id.view_venue_detail);
        mHeaderImageView = (ImageView) findViewById(R.id.image);
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        findViewById(R.id.tv_detail_save).setOnClickListener(this);
        findViewById(R.id.tv_detail_share).setOnClickListener(this);
    }


    private void init() {
        adjustHeaderImageDimen();
        if (getIntent() != null) {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                mPlaceId = bundle.getString(AppConstant.BUNDLE_PLACE_ID);
                String imageUrl = bundle.getString(AppConstant.BUNDLE_IMAGE_URL);
                if (!TextUtils.isEmpty(imageUrl))
                    imageLoader.displayImage(imageUrl, mHeaderImageView);
            }
        }
    }

    private void initVenueInfo() {
        int distanceType = SmartCityApplication.getAppInstance().getDistanceType();
        String distanceStr;
        if (distanceType == AppConstant.KILOMETER_TYPE)
            distanceStr = AppConstant.KILOMETER;
        else
            distanceStr = AppConstant.MILE;
        final PlaceSearchInfo info = SmartCityApplication.getAppInstance().getDetailsInfo();
        if (info != null) {
            TextView nameTextView = (TextView) findViewById(R.id.tv_place_name);
            TextView addressTextView = (TextView) findViewById(R.id.tv_place_address);
            TextView distanceTextView = (TextView) findViewById(R.id.tv_distance);
            ImageView venueImageView = (ImageView) findViewById(R.id.iv_venue);
            TextView ratingTextView = (TextView) findViewById(R.id.tv_rating);
            TextView voteTextView = (TextView) findViewById(R.id.tv_vote);
            TextView categoryTextView = (TextView) findViewById(R.id.tv_category);
            TextView contactTextView = (TextView) findViewById(R.id.tv_place_contact);

            nameTextView.setText(info.getName());
            String city = info.getCityName();
            if (!TextUtils.isEmpty(city)) {
                String add = info.getAddress();
                if (TextUtils.isEmpty(add))
                    add = "";
                String address = String.format("%s, %s", add, city);
                addressTextView.setText(address);
            } else if (!TextUtils.isEmpty(info.getAddress())) {
                addressTextView.setText(info.getAddress());
            }
            if (info.getDistance() > 0) {
                mDistance = String.format("%s %s", info.getDistance(), distanceStr);
                distanceTextView.setText(mDistance);
            } else if (!TextUtils.isEmpty(info.getDistanceStr())) {
                mDistance = info.getDistanceStr();
                distanceTextView.setText(info.getDistanceStr());
            }
            if (!TextUtils.isEmpty(info.getContactNumber())) {
                contactTextView.setText(info.getContactNumber());
                findViewById(R.id.view1).setVisibility(View.VISIBLE);
            } else
                contactTextView.setVisibility(View.GONE);
            String category = info.getCategory();
            if (!TextUtils.isEmpty(category))
                categoryTextView.setText(category);
            int vote = info.getVote();
            if (vote > 0) {
                String votes = String.format("%s votes", vote);
                voteTextView.setText(votes);
            } else
                voteTextView.setVisibility(View.INVISIBLE);

            float rating = info.getRatingInt();
            if (rating > 0) {
                findViewById(R.id.iv_foursquare).setVisibility(View.VISIBLE);
                ratingTextView.setText(String.valueOf(rating));
            } else if (!TextUtils.isEmpty(info.getRating())) {
                findViewById(R.id.iv_foursquare).setVisibility(View.VISIBLE);
                ratingTextView.setText(info.getRating());

            } else
                ratingTextView.setVisibility(View.INVISIBLE);
            String imageUrl = info.getIconUrl();
            if (!TextUtils.isEmpty(imageUrl)) {
                imageLoader.displayImage(imageUrl, venueImageView, options, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String s, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String s, View view, FailReason failReason) {
                        setDefaultActionTheme();
                    }

                    @Override
                    public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                        mHeaderImageView.setImageBitmap(bitmap);
                        setPalette();
                    }

                    @Override
                    public void onLoadingCancelled(String s, View view) {
                    }
                });
            } else
                setDefaultActionTheme();
            contactTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String number = info.getContactNumber();
                    if(!TextUtils.isEmpty(number)) {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        String contact = String.format("tel:%s", number);
                        intent.setData(Uri.parse(contact));
                        startActivity(intent);
                    }
                }
            });
        }

    }


    private void adjustHeaderImageDimen() {
        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth(); // ((display.getWidth()*20)/100)
        int height = (display.getHeight() * 70) / 100;// ((display.getHeight()*30)/100)
        LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(width, height);
        mHeaderImageView.setLayoutParams(parms);
    }


    private void initDetailFragment(PlaceDetailsInfo info) {
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        mVenueDetailFragment = VenueDetailFragment.getInstance(info);
        fragmentTransaction.add(R.id.container_venue_detail, mVenueDetailFragment);
        fragmentTransaction.commit();
    }


    private void getVenueDetail() {

        showProgress();
        String url = Url.getFoursquareVenueDetailUrl(mPlaceId);
        OperationManager.getInstance().getPlaceDetailsList(url, this);

    }

    private void setPalette() {
        final int primaryDark = getResources().getColor(R.color.theme_color);
        final int primary = getResources().getColor(R.color.primary);
        if (mHeaderImageView.getDrawable() != null) {
            Bitmap bitmap = ((BitmapDrawable) mHeaderImageView.getDrawable()).getBitmap();
            Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
                @Override
                public void onGenerated(Palette palette) {
                    int contentColor = palette.getMutedColor(primaryDark);
                    int alphaColor = getColorWithAlpha(contentColor, .9f);
                    collapsingToolbarLayout.setContentScrimColor(alphaColor);
                    collapsingToolbarLayout.setStatusBarScrimColor(palette.getDarkVibrantColor(primary));
                    parentView.setBackgroundColor(contentColor);
                }
            });
        } else {
            setDefaultActionTheme();
        }

    }

    private void setDefaultActionTheme() {
        final int primaryDark = getResources().getColor(R.color.theme_color);
        final int primary = getResources().getColor(R.color.primary);
        mHeaderImageView.setVisibility(View.GONE);
        collapsingToolbarLayout.setContentScrimColor(primaryDark);
        collapsingToolbarLayout.setStatusBarScrimColor(primary);
        parentView.setBackgroundColor(primaryDark);
    }

    @Override
    public void onSuccessGetDetails(PlaceDetailsInfo detailsInfos) {
        hideProgress();
        if (detailsInfos != null) {
            mDetailsInfo = detailsInfos;
            mVenueDetailFragment.setData(detailsInfos);
            insertDetailsInfo(detailsInfos);
            saveVenueHistoryInDB();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showInterstitialAdd();
                }
            }, 2000);
        }
    }

    @Override
    public void onFailureGetDetails(com.positiveminds.smartcity.model.entity.Error error) {
        hideProgress();
        if (error != null && !TextUtils.isEmpty(error.getMessage())) {
            showSnackBar(error.getMessage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_venue_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void insertDetailsInfo(PlaceDetailsInfo detailsInfo) {
        TextView openStatusTextView = (TextView) findViewById(R.id.tv_open_staus);
        if (!TextUtils.isEmpty(detailsInfo.getOpenStatus())) {
            findViewById(R.id.view1).setVisibility(View.VISIBLE);
            openStatusTextView.setVisibility(View.VISIBLE);
            openStatusTextView.setText(detailsInfo.getOpenStatus());
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_detail_save:
                askFavouriteAlert(AppConstant.VENUE_FAVOURITE_TYPE);
                break;
            case R.id.tv_detail_share:
                shareVenue();
                break;

        }
    }

    private void saveVenueHistoryInDB() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                saveVenueDetailInDB(AppConstant.VENUE_HISTORY_TYPE);
                return null;
            }

            @Override
            protected void onPostExecute(String aVoid) {
                super.onPostExecute(aVoid);
            }
        }.execute();
    }

    private void shareVenue() {
        new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Void... params) {
                View view = findViewById(R.id.cv);
                if (view != null) {
                    Bitmap bitmap = getBitmapFromView(view);
                    return bitmap;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap bitmapPath) {
                super.onPostExecute(bitmapPath);
                if (bitmapPath != null) {
                    String path = saveImage(bitmapPath);
                    if (!TextUtils.isEmpty(path)) {
                        Uri uriToImage = Uri.fromFile(new File(path));
                        Intent shareIntent = new Intent();
                        shareIntent.setAction(Intent.ACTION_SEND);
                        shareIntent.putExtra(Intent.EXTRA_STREAM, uriToImage);
                        shareIntent.setType("image/jpeg");
                        startActivity(Intent.createChooser(shareIntent, "Share Card to.."));
                    }
                }
            }
        }.execute();
    }

    private static Bitmap getBitmapFromView(View view) {
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return returnedBitmap;
    }

    private String saveImage(Bitmap finalBitmap) {
        String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File myDir = new File(root + "/Cards");
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
        String iname = "card" + ".jpg";
        File file = new File(myDir, iname);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mCardImagePath = root + "/Cards/" + iname;
        return mCardImagePath;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        showInterstitialAdd();
        if (!TextUtils.isEmpty(mCardImagePath)) {
            File file = new File(mCardImagePath);
            if (file.exists())
                file.delete();
        }
    }

    private void askFavouriteAlert(final int venueType) {
        new SweetAlertDialog(this, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText("Add to Favourite List")
                .setContentText("Would you like to add this result in to Favourite List.").setCancelText("Not now!")
                .setConfirmText(getResources().getString(R.string.yes_text)).showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();
                        saveVenueDetailInDB(venueType);
                    }
                }).show();
    }

    private void saveVenueDetailInDB(int venueType) {
        if (mDetailsInfo != null) {
            ContentValues cv = new ContentValues();
            cv.put(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_NAME, mDetailsInfo.getPlaceName());
            cv.put(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_ID, mPlaceId);
            cv.put(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_ADDRESS, mDetailsInfo.getAddress());
            cv.put(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_CATEGORY, mDetailsInfo.getCategory());
            if (!TextUtils.isEmpty(mDistance))
                cv.put(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_DISTANCE, mDistance);
            cv.put(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_URL, mDetailsInfo.getImageUrl());
            String rating = mDetailsInfo.getRating() > 0 ? String.format("%s", mDetailsInfo.getRating()) : "";
            cv.put(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_RATING, rating);
            cv.put(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_VOTE, mDetailsInfo.getVote());
            cv.put(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_VENUE_TYPE, venueType);
            LatLng location = mDetailsInfo.getLocation();
            if (location != null) {
                cv.put(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_LAT, String.valueOf(location.latitude));
                cv.put(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_LONG, String.valueOf(location.longitude));
            }
            int result = DBManager.getInstance().update(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_ID, mPlaceId, FavouritePlacesContract.PlaceEntry.TABLE_NAME, cv);
            if (result >= 0 && venueType == AppConstant.VENUE_FAVOURITE_TYPE)
                showSnackBar("Added Successfully");
        }

    }

    @Override
    public void showMessage(String message) {
        showSnackBar(message);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        boolean result = super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.action_visit:
                Intent intent = new Intent(VenueDetailActivity.this, VenueMapActivity.class);
                intent.putExtra(AppConstant.BUNDLE_PLACE_NAME, mDetailsInfo.getPlaceName());
                intent.putExtra(AppConstant.BUNDLE_PLACE_ADD, mDetailsInfo.getAddress());
                intent.putExtra(AppConstant.BUNDLE_PLACE_LAT, mDetailsInfo.getLocation().latitude);
                intent.putExtra(AppConstant.BUNDLE_PLACE_LNG, mDetailsInfo.getLocation().longitude);
                startActivity(intent);
                result = true;
                break;
            case R.id.action_save:
                askFavouriteAlert(AppConstant.VENUE_FAVOURITE_TYPE);
                result = true;
                break;
            case R.id.action_share:
                shareVenue();
                result = true;
                break;
        }
        return result;
    }

    public static int getColorWithAlpha(int color, float ratio) {
        int newColor = 0;
        int alpha = Math.round(Color.alpha(color) * ratio);
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);
        newColor = Color.argb(alpha, r, g, b);
        return newColor;
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
