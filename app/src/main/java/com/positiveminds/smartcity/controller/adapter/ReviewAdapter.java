package com.positiveminds.smartcity.controller.adapter;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.model.constant.AppConstant;
import com.positiveminds.smartcity.model.entity.UserReview;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by RajeevTomar on 9/7/2015.
 */
public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder> {

    List<UserReview> mUserReviewList;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;

    public ReviewAdapter(List<UserReview> userReviewList)
    {
        this.mUserReviewList = userReviewList;
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_review_list, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        UserReview review = mUserReviewList.get(position);
        if(review != null)
        {
            String name = review.getUserName();
            if(!TextUtils.isEmpty(name))
                holder.nameTextView.setText(name);
            String imageUrl = review.getProfileUrl();
            if(!TextUtils.isEmpty(imageUrl))
                imageLoader.displayImage(imageUrl,holder.profileImageView,options);
            String text = review.getReviewMsg();
            if(!TextUtils.isEmpty(text))
                holder.reviewTextView.setText(text);
        }
    }

    @Override
    public int getItemCount() {
        return mUserReviewList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTextView;
        public TextView reviewTextView;
        public ImageView profileImageView;

        public ViewHolder(View v) {
            super(v);
            nameTextView = (TextView)v.findViewById(R.id.tv_review_name);
            profileImageView = (ImageView)v.findViewById(R.id.iv_review_profile);
            reviewTextView = (TextView)v.findViewById(R.id.tv_review_text);
        }
    }


}
