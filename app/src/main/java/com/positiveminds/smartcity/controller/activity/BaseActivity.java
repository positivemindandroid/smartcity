package com.positiveminds.smartcity.controller.activity;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.widget.SweetAlertDialog;

/**
 * Created by RajeevTomar on 7/24/2015.
 */
public class BaseActivity extends AppCompatActivity {


    protected View mProgressBar;
    private AdView mAdView;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initInterstitialAd();
    }

    private void initInterstitialAd() {
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.interstitial_id));
        requestNewInterstitial();
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("AE9918E00C1557F4DFA097EEDF52D889").build();
        mInterstitialAd.loadAd(adRequest);
    }

    protected void showProgress() {
        if (mProgressBar != null)
            mProgressBar.setVisibility(View.VISIBLE);
    }

    protected void hideProgress() {
        if (mProgressBar != null)
            mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void launchGoogleMapApp(String query) {
        Uri gmmIntentUri = Uri.parse(query);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    protected boolean isInternetConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    protected void openLocationDialog() {
        showSnackBar("Location is off");
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Location is off!")
                .setContentText("Please enable loaction to get places near by.").setCancelText("Not now!")
                .setConfirmText(getResources().getString(R.string.got_text)).showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                }).show();
    }

    protected static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    protected void showNoInternetDialog() {
        showSnackBar("Internet connection not found");
        new SweetAlertDialog(BaseActivity.this, SweetAlertDialog.WARNING_TYPE).setTitleText("Can't Connect!")
                .setContentText("Please check your network connection").setConfirmText("Go to Setting")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();
                        Intent intent = new Intent(Settings.ACTION_SETTINGS);
                        startActivity(intent);
                    }
                }).show();

    }

    protected  void showNoResultFoundAlert(String title, String message)
    {
        showSnackBar(title);
        new SweetAlertDialog(BaseActivity.this, SweetAlertDialog.WARNING_TYPE).setTitleText(title)
                .setContentText(message).setConfirmText("Go Back")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();
                        finish();
                    }
                }).show();
    }


    protected void showSnackBar( String message)
    {
        getSnackBar(message).show();
    }


    protected  void showSnackBar(String message, String actionText, View.OnClickListener actionClickListener)
    {
        Snackbar snackbar = getSnackBar(message);
        if(actionText != null)
            snackbar.setAction(actionText,actionClickListener);
        snackbar.show();
    }


    private Snackbar getSnackBar( String message)
    {
        Snackbar snackbar  = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(getResources().getColor(R.color.theme_color));
        return snackbar;
    }

    protected void initAdView(int addViewId) {
        // enable for app add
		mAdView = (AdView) findViewById(addViewId);
		AdRequest adRequest = new AdRequest.Builder().addTestDevice("AE9918E00C1557F4DFA097EEDF52D889").build();
		mAdView.loadAd(adRequest);
    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    protected void showInterstitialAdd() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();

            }
        });
    }

}
