package com.positiveminds.smartcity.controller.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.SmartCityApplication;
import com.positiveminds.smartcity.controller.DBManager;
import com.positiveminds.smartcity.controller.OperationManager;
import com.positiveminds.smartcity.model.Listener.PlaceDetailsOperationListener;
import com.positiveminds.smartcity.model.constant.AppConstant;
import com.positiveminds.smartcity.model.db.FavouritePlacesContract;
import com.positiveminds.smartcity.model.entity.PlaceDetailsInfo;
import com.positiveminds.smartcity.model.server.Url;
import com.positiveminds.smartcity.widget.SweetAlertDialog;

/**
 * Created by RajeevTomar on 7/27/2015.
 */
public class PlaceDetailActivity extends BaseActivity implements PlaceDetailsOperationListener, View.OnClickListener {

    private String mPlaceId;
    private TextView mNameTextView;
    private TextView mAddressTextView;
    private Button mPhoneNumberBtn;
    private Button mWebsiteBtn;
    private Button mMapUrlBtn;
    private GoogleMap mGoogleMap;
    private PlaceDetailsInfo mDetailsInfo;
    private String mPlaceType;
    private RatingBar mRatingBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    private void init() {
        Typeface myTypeface = SmartCityApplication.getAppInstance().getPrimaryTypeface();
        setUpMapIfNeeded();
        mProgressBar = findViewById(R.id.bar_details_activity);
        mNameTextView = (TextView) findViewById(R.id.tv_detail_name);
        mNameTextView.setTypeface(myTypeface);
        mAddressTextView = (TextView) findViewById(R.id.tv_detail_address);
        mPhoneNumberBtn = (Button) findViewById(R.id.tv_details_call);
        mPhoneNumberBtn.setOnClickListener(this);
        mWebsiteBtn = (Button) findViewById(R.id.tv_details_website);
        mWebsiteBtn.setOnClickListener(this);
        mMapUrlBtn = (Button) findViewById(R.id.tv_details_mapurl);
        mMapUrlBtn.setOnClickListener(this);
        mRatingBar = (RatingBar) findViewById(R.id.place_rating);
        findViewById(R.id.iv_direction).setOnClickListener(this);
        findViewById(R.id.iv_favourite).setOnClickListener(this);
        if (getIntent() != null) {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                mPlaceId = bundle.getString(AppConstant.BUNDLE_PLACE_ID);
                mPlaceType = bundle.getString(AppConstant.BUNDLE_PLACE_TYPE);
                float distance = bundle.getFloat(AppConstant.BUNDLE_DISTANCE);
                if (distance > 0) {
                    String distanceType;
                    if (SmartCityApplication.getAppInstance().getDistanceType() == AppConstant.KILOMETER_TYPE) {
                        distanceType = AppConstant.KILOMETER;
                    } else
                        distanceType = AppConstant.MILE;
                    String distanceStr = String.format("%s %s", distance, distanceType);
                    if (!TextUtils.isEmpty(distanceStr)) {
                        TextView textView = (TextView) findViewById(R.id.tv_details_distance);
                        textView.setTypeface(SmartCityApplication.getAppInstance().getPrimaryTypeface());
                        textView.setText(distanceStr);
                    }
                }
                if (!TextUtils.isEmpty(mPlaceType))
                    getSupportActionBar().setTitle(mPlaceType);
            }
        }
    }

    private void setUpMapIfNeeded() {
        if (mGoogleMap != null) {
            return;
        }
        mGoogleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        if (mPlaceId != null) {
            getPlaceDetails();
        }
    }

    private void getPlaceDetails() {
        showProgress();
        String url = Url.getFoursquareVenueDetailUrl(mPlaceId);
        OperationManager.getInstance().getPlaceDetailsList(url, this);
    }


    @Override
    public void onSuccessGetDetails(PlaceDetailsInfo detailsInfos) {
        if (detailsInfos != null) {
            if (mGoogleMap != null) {
                LatLng location = detailsInfos.getLocation();
                if (location != null) {
                    Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(location.latitude, location.longitude))
                            .title(detailsInfos.getPlaceName()));
                    marker.showInfoWindow();
                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.latitude, location.longitude), 16f));
                }
            }
            mDetailsInfo = detailsInfos;
            hideProgress();

            String name = detailsInfos.getPlaceName();
            if (name != null)
                mNameTextView.setText(name);
            String address = detailsInfos.getAddress();
            if (address != null)
                mAddressTextView.setText(address);
            String phoneNo = detailsInfos.getPhoneNumber();
            if (!TextUtils.isEmpty(phoneNo)) {
                mPhoneNumberBtn.setVisibility(View.VISIBLE);
            }
            double rating = detailsInfos.getRating();
            if (rating > 0) {
                mRatingBar.setVisibility(View.VISIBLE);
                mRatingBar.setRating((float) rating);
            }
            String websiteLink = detailsInfos.getWebsite();
            if (!TextUtils.isEmpty(websiteLink)) {
                mWebsiteBtn.setVisibility(View.VISIBLE);

            }
            String mapUrl = detailsInfos.getMapUrl();
            if (!TextUtils.isEmpty(mapUrl)) {
                mMapUrlBtn.setVisibility(View.VISIBLE);

            }
        }
    }

    @Override
    public void onFailureGetDetails(com.positiveminds.smartcity.model.entity.Error error) {
        hideProgress();
        if (error != null) {
            showSnackBar(error.getMessage());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_details_call:
                String phoneNumber = mDetailsInfo.getPhoneNumber();
                if (!TextUtils.isEmpty(phoneNumber)) {
                    phoneNumber = phoneNumber.replace(" ", "");
                    String number = "tel:" + phoneNumber.toString().trim();
                    Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
                    startActivity(callIntent);
                }
                break;
            case R.id.tv_details_website:
                String websiteUrl = mDetailsInfo.getWebsite();
                if (!TextUtils.isEmpty(websiteUrl)) {
                    openExternalBrowser(websiteUrl);
                }
                break;
            case R.id.tv_details_mapurl:
                String mapUrl = mDetailsInfo.getMapUrl();
                if (!TextUtils.isEmpty(mapUrl)) {
                    //"http://maps.google.com/maps/place?cid=10281119596374313554
                    Uri uri = Uri.parse(mapUrl);
                    String server = uri.getAuthority();
                    if (server.equals("maps.google.com")) {
                        launchGoogleMapApp(mapUrl);
                    } else {
                        openExternalBrowser(mapUrl);
                    }
                }
                break;
            case R.id.iv_direction:
                if (mDetailsInfo != null) {
                    LatLng location = mDetailsInfo.getLocation();
                    if (location != null) {
                        String query = String.format("google.navigation:q=%s,%s", location.latitude, location.longitude);
                        launchGoogleMapApp(query);
                    }
                }
                break;
            case R.id.iv_favourite:
                askFavouriteAlert();
                break;
        }
    }

    private void openExternalBrowser(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_action_map, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_map:
                if (mDetailsInfo != null) {
                    String query = String.format("geo:%s,%s?z=10&q=%s", mDetailsInfo.getLocation().latitude, mDetailsInfo.getLocation().longitude, mDetailsInfo.getPlaceName());
                    launchGoogleMapApp(query);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void askFavouriteAlert()
    {
        new SweetAlertDialog(this, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText("Add to Favourite List")
                .setContentText("Would you like to add this result in to Favourite List.").setCancelText("Not now!")
                .setConfirmText(getResources().getString(R.string.yes_text)).showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();
                        if (mDetailsInfo != null) {
                            LatLng location = mDetailsInfo.getLocation();
                            ContentValues cv = new ContentValues();
                            cv.put(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_NAME, mDetailsInfo.getPlaceName());
                            cv.put(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_ID, mPlaceId);
                            cv.put(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_ADDRESS, mDetailsInfo.getAddress());
                            cv.put(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_LAT, String.valueOf(location.latitude));
                            cv.put(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_LONG, String.valueOf(location.longitude));
                            int result = DBManager.getInstance().update(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_ID, mPlaceId, FavouritePlacesContract.PlaceEntry.TABLE_NAME, cv);
                            if (result >= 0)
                                showSnackBar("Added Successfully");
                        }
                    }
                }).show();
    }
}
