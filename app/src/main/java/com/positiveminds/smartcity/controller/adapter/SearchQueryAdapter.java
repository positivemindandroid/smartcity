package com.positiveminds.smartcity.controller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.positiveminds.smartcity.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RajeevTomar on 8/31/2015.
 */
public class SearchQueryAdapter extends  HomeAdapter {

    public SearchQueryAdapter(List<String> searchInfoList, Context context) {
        super(searchInfoList,context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_search_query, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }
}
