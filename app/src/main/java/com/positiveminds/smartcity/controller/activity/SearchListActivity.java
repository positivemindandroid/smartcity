package com.positiveminds.smartcity.controller.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.SmartCityApplication;
import com.positiveminds.smartcity.controller.adapter.SearchListAdapter;
import com.positiveminds.smartcity.model.Listener.RecyclerItemClickListener;
import com.positiveminds.smartcity.model.constant.AppConstant;
import com.positiveminds.smartcity.model.entity.PlaceSearchInfo;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by RajeevTomar on 7/27/2015.
 */
public class SearchListActivity extends BaseActivity {

    private List<PlaceSearchInfo> mSearchInfoList;
    private String mPlaceType;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    private void init() {
        if (getIntent() != null) {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                mPlaceType = bundle.getString(AppConstant.BUNDLE_PLACE_TYPE);
                if(!TextUtils.isEmpty(mPlaceType))
                {
                    getSupportActionBar().setTitle(mPlaceType);
                }
            }
        }
        mProgressBar = findViewById(R.id.bar_search_list_activity);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.search_list_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mSearchInfoList = SmartCityApplication.getAppInstance().getSearchInfoList();
        if (mSearchInfoList != null && mSearchInfoList.size() > 0) {
            SearchListAdapter listAdapter = new SearchListAdapter(mSearchInfoList);
            recyclerView.setAdapter(listAdapter);
        }
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(SearchListActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (isInternetConnected()) {
                            PlaceSearchInfo searchInfo = mSearchInfoList.get(position);
                            if (searchInfo != null) {
                                String placeId = searchInfo.getPlaceId();
                                if (placeId != null) {
                                    Intent intent = new Intent(SearchListActivity.this, VenueDetailActivity.class);
                                    SmartCityApplication.getAppInstance().setDetailsInfo(searchInfo);
                                    intent.putExtra(AppConstant.BUNDLE_PLACE_ID, placeId);
                                    intent.putExtra(AppConstant.BUNDLE_IMAGE_URL, searchInfo.getIconUrl());
                                    startActivity(intent);
                                }
                            }
                        } else
                            showNoInternetDialog();
                    }
                })
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_action_map, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_map:
                if(!TextUtils.isEmpty(mPlaceType)) {
                    String query = String.format("geo:0,0?q=%s", mPlaceType);
                    launchGoogleMapApp(query);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
