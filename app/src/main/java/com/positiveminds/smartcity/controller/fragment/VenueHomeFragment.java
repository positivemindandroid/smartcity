package com.positiveminds.smartcity.controller.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.SmartCityApplication;
import com.positiveminds.smartcity.controller.OperationManager;
import com.positiveminds.smartcity.controller.activity.SearchActivity;
import com.positiveminds.smartcity.controller.activity.VenueDetailActivity;
import com.positiveminds.smartcity.controller.adapter.ProductAdapter;
import com.positiveminds.smartcity.controller.adapter.SearchListAdapter;
import com.positiveminds.smartcity.model.Listener.PlaceSearchOperationListener;
import com.positiveminds.smartcity.model.Listener.RecyclerItemClickListener;
import com.positiveminds.smartcity.model.constant.AppConstant;
import com.positiveminds.smartcity.model.entity.PlaceDetailsInfo;
import com.positiveminds.smartcity.model.entity.PlaceSearchInfo;
import com.positiveminds.smartcity.model.entity.Product;
import com.positiveminds.smartcity.model.server.Url;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RajeevTomar on 9/8/2015.
 */
public class VenueHomeFragment extends BaseFragment {

    private View mView;
    RecyclerView recyclerView;
    private ProductAdapter adapter;
    private List<Product> data = new ArrayList<>();
    private List<PlaceSearchInfo> mSearchInfoList;
    SearchListAdapter listAdapter;
    public static VenueHomeFragment getInstance() {
        VenueHomeFragment fragment = new VenueHomeFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_venue_home, null);
        initView();
        init();
        return mView;
    }

    private void initView() {
        adapter = new ProductAdapter(getActivity(), data);
        recyclerView = (RecyclerView) mView.findViewById(R.id.home_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        listAdapter = new SearchListAdapter(new ArrayList<PlaceSearchInfo>());
        recyclerView.setAdapter(listAdapter);
        EditText searchEditText = (EditText) mView.findViewById(R.id.et_search_home);
        searchEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SearchActivity.class));
            }
        });
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (true) {
                            PlaceSearchInfo searchInfo = mSearchInfoList.get(position);
                            if (searchInfo != null) {
                                String placeId = searchInfo.getPlaceId();
                                if (placeId != null) {
                                    Intent intent = new Intent(getActivity(), VenueDetailActivity.class);
                                    SmartCityApplication.getAppInstance().setDetailsInfo(searchInfo);
                                    intent.putExtra(AppConstant.BUNDLE_PLACE_ID, placeId);
                                    intent.putExtra(AppConstant.BUNDLE_IMAGE_URL, searchInfo.getIconUrl());
                                    startActivity(intent);
                                }
                            }
                        }
                    }
                })
        );
    }

    private void init()
    {
        getSearchList();
    }


    private void getSearchList() {
        showProgress();
        LatLng currentLocation = SmartCityApplication.getAppInstance().getCurrentLocation();
        String url = Url.getFoursquarePlaceSearchUrl("", currentLocation, "topPicks");
        OperationManager.getInstance().getSearchPlaceList(url, new PlaceSearchOperationListener() {
            @Override
            public void onSuccessPlaceSearch(List<PlaceSearchInfo> searchInfoList) {
                mSearchInfoList = searchInfoList;
                listAdapter.loadData(searchInfoList);
                hideProgress();
            }

            @Override
            public void onFailurePlaceSearch(com.positiveminds.smartcity.model.entity.Error error) {
                hideProgress();
            }
        });
    }

}
