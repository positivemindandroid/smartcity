package com.positiveminds.smartcity.controller.fragment;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ProgressBar;


/**
 * Created by RajeevTomar on 7/27/2015.
 */
public class BaseFragment extends Fragment {

    protected View mProgressBar;
    protected void showProgress()
    {
        if(mProgressBar != null)
            mProgressBar.setVisibility(View.VISIBLE);
    }

    protected void hideProgress()
    {
        if(mProgressBar != null)
            mProgressBar.setVisibility(View.GONE);
    }

    protected void launchGoogleMapApp(String query) {
        Uri gmmIntentUri = Uri.parse(query);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }
}
