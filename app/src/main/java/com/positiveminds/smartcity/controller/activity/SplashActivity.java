package com.positiveminds.smartcity.controller.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.positiveminds.smartcity.R;


public class SplashActivity extends Activity {

    private static final int LAUNCH_DELAY_TIME = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_splash);
        launchHomeScreen();
    }

    private void launchHomeScreen() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, FoursquareHomeActivity.class));
                finish();
            }
        }, LAUNCH_DELAY_TIME);
    }


}
