package com.positiveminds.smartcity.controller;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.google.android.gms.location.places.Place;
import com.positiveminds.smartcity.SmartCityApplication;
import com.positiveminds.smartcity.model.Listener.DBOperationListener;
import com.positiveminds.smartcity.model.db.DBHelper;
import com.positiveminds.smartcity.model.db.FavouritePlacesContract;

import java.util.List;

/**
 * Created by RajeevTomar on 8/19/2015.
 */
public class DBManager {

    private static SQLiteDatabase sqLiteDatabase;
    private static DBManager dbManagerInstance;

    public static DBManager getInstance() {
        if (sqLiteDatabase == null) {
            DBHelper dbHelper = new DBHelper(SmartCityApplication.getAppInstance().getApplicationContext());
            sqLiteDatabase = dbHelper.getWritableDatabase();
            dbManagerInstance = new DBManager();
        }
        return dbManagerInstance;
    }

    public long insert(String tableName, ContentValues contentValues) {
        if (sqLiteDatabase != null) {
            return sqLiteDatabase.insert(tableName, null, contentValues);
        }
        return -1;
    }

    public int update(String key, String value, String tableName, ContentValues contentValues) {
        int result = -1;
        if (sqLiteDatabase != null) {

            String selection = key + " LIKE ?";
            String[] selectionArgs = {value};
            result = sqLiteDatabase.update(tableName, contentValues, selection, selectionArgs);
            if (result <= 0) {
                result = (int) insert(tableName, contentValues);
            }
        }
        return result;
    }

    public int delete(String key, String value, String tableName) {
        if (sqLiteDatabase != null) {
            String selection = key + " LIKE ?";
            String[] selectionArgs = {value};
            return sqLiteDatabase.delete(tableName, selection, selectionArgs);
        }
        return -1;
    }

    public void closeDatabase() {
        if (sqLiteDatabase != null)
            sqLiteDatabase.close();
    }

    public void readDB(String tableName, String[] projection, String selection, String[] selectionArgs, String sortOrder, DBOperationListener listener) {
        if (sqLiteDatabase != null) {
            try {
                Cursor c = sqLiteDatabase.query(
                        tableName,  // The table to query
                        projection,                               // The columns to return
                        selection,                                // The columns for the WHERE clause
                        selectionArgs,                            // The values for the WHERE clause
                        null,                                     // don't group the rows
                        null,                                     // don't filter by row groups
                        sortOrder                                 // The sort order
                );
                listener.onSuccessReadDB(c);
            }
            catch (SQLiteException exception)
            {
                if(exception != null)
                    listener.onFailureDB(exception.getMessage());
            }

        }
    }

}
