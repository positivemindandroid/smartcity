package com.positiveminds.smartcity.controller.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.SmartCityApplication;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RajeevTomar on 7/24/2015.
 */
public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    private List<String> mPlaceTypeList;
    private static Context mContext;


    public HomeAdapter(List<String> searchInfoList, Context context) {
        this.mPlaceTypeList = new ArrayList<>(searchInfoList);
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_home_grid, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String placeType = mPlaceTypeList.get(position);
        holder.mTextView.setText(placeType);
    }

    @Override
    public int getItemCount() {
        return mPlaceTypeList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;

        public ViewHolder(View v) {
            super(v);
            Typeface myTypeface = SmartCityApplication.getAppInstance().getPrimaryTypeface();
            mTextView = (TextView) v.findViewById(R.id.tv_place_type);
            mTextView.setTypeface(myTypeface);

        }

    }


    public void animateTo(List<String> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    private void applyAndAnimateRemovals(List<String> newModels) {
        for (int i = mPlaceTypeList.size() - 1; i >= 0; i--) {
            final String model = mPlaceTypeList.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<String> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final String model = newModels.get(i);
            if (!mPlaceTypeList.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<String> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final String model = newModels.get(toPosition);
            final int fromPosition = mPlaceTypeList.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public String removeItem(int position) {
        final String model = mPlaceTypeList.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, String model) {
        mPlaceTypeList.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final String model = mPlaceTypeList.remove(fromPosition);
        mPlaceTypeList.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

}
