package com.positiveminds.smartcity.controller.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.positiveminds.smartcity.R;


public class MapFragment extends Fragment {

	private View mView;
	private GoogleMap mGoogleMap;
	private MapFragmentListener mListener;

	public interface MapFragmentListener {
		public void onClickMarker(LatLng latLong);
		public void onMapLoad(GoogleMap map);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = inflater.inflate(R.layout.fragment_map, null);
		init();
		return mView;

	}

	private void init() {
		mGoogleMap = ((SupportMapFragment) getChildFragmentManager()
				.findFragmentById(R.id.map)).getMap();
		mListener.onMapLoad(mGoogleMap);
	}

	public void setListener(MapFragmentListener listener) {
		this.mListener = listener;
	}

	public GoogleMap getMap() {
		return mGoogleMap;
	}

}
