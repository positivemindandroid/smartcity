package com.positiveminds.smartcity.controller.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.SmartCityApplication;
import com.positiveminds.smartcity.controller.activity.VenueDetailActivity;
import com.positiveminds.smartcity.controller.activity.VenueListScreen;
import com.positiveminds.smartcity.controller.activity.VenueMapActivity;
import com.positiveminds.smartcity.controller.adapter.ReviewAdapter;
import com.positiveminds.smartcity.controller.adapter.SearchListAdapter;
import com.positiveminds.smartcity.model.constant.AppConstant;
import com.positiveminds.smartcity.model.entity.PlaceDetailsInfo;
import com.positiveminds.smartcity.model.entity.Product;
import com.positiveminds.smartcity.model.entity.UserReview;

import java.util.List;
import java.util.Locale;

/**
 * Created by RajeevTomar on 7/27/2015.
 */
public class VenueDetailFragment extends BaseFragment implements View.OnClickListener {

    private View mView;
    private GoogleMap mGoogleMap;
    private PlaceDetailsInfo mDeatilInfo;
    private RecyclerView recyclerView;
    private VenueDetailFragmentListener listener;

    public interface  VenueDetailFragmentListener
    {
        public void showMessage(String message);
    }



    public static VenueDetailFragment getInstance(PlaceDetailsInfo info) {
        VenueDetailFragment fragment = new VenueDetailFragment();
        fragment.mDeatilInfo = info;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_venue_detail, null);
        mProgressBar = mView.findViewById(R.id.bar_venue_fragment);
        initView();
        init();
        return mView;

    }

    private void initView() {
        setUpMapIfNeeded();
        recyclerView = (RecyclerView) mView.findViewById(R.id.venue_detail_recycler);
        recyclerView.setVerticalScrollBarEnabled(false);
        recyclerView.stopScroll();
        recyclerView.setEnabled(false);
        recyclerView.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        showProgress();
    }

    private void reloadList() {
        if (mDeatilInfo != null) {
            List<UserReview> userReviews = mDeatilInfo.getReviewList();
            if (userReviews != null && userReviews.size() > 0) {
                ReviewAdapter listAdapter = new ReviewAdapter(userReviews);
                recyclerView.setAdapter(listAdapter);
            }
        }

    }


    private void init() {

        mView.findViewById(R.id.iv_direction).setOnClickListener(this);
        mView.findViewById(R.id.tv_more_review).setOnClickListener(this);
        mView.findViewById(R.id.rl_nearby).setOnClickListener(this);
        mView.findViewById(R.id.rl_restaurants).setOnClickListener(this);
        mView.findViewById(R.id.tv_website).setOnClickListener(this);
        mView.findViewById(R.id.tv_google_maps).setOnClickListener(this);
        mView.findViewById(R.id.tv_foursquare).setOnClickListener(this);
        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Intent intent = new Intent(getActivity(), VenueMapActivity.class);
                intent.putExtra(AppConstant.BUNDLE_PLACE_NAME, mDeatilInfo.getPlaceName());
                intent.putExtra(AppConstant.BUNDLE_PLACE_ADD, mDeatilInfo.getAddress());
                intent.putExtra(AppConstant.BUNDLE_PLACE_LAT, mDeatilInfo.getLocation().latitude);
                intent.putExtra(AppConstant.BUNDLE_PLACE_LNG, mDeatilInfo.getLocation().longitude);
                startActivity(intent);
            }
        });
    }


    private void setUpMapIfNeeded() {
        if (mGoogleMap != null) {
            return;
        }
        mGoogleMap = ((SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map)).getMap();
        mGoogleMap.getUiSettings().setScrollGesturesEnabled(false);
    }

    public void setData(PlaceDetailsInfo info) {
        hideProgress();
        if (info != null) {
            mDeatilInfo = info;
            reloadList();
            if (mGoogleMap != null) {
                LatLng location = info.getLocation();
                if (location != null) {
                    Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(location.latitude, location.longitude))
                            .title(info.getPlaceName()));
                    marker.showInfoWindow();
                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.latitude, location.longitude), 16f));
                }
            }

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.map:
                Intent intent = new Intent(getActivity(), VenueMapActivity.class);
                intent.putExtra(AppConstant.BUNDLE_PLACE_NAME, mDeatilInfo.getPlaceName());
                intent.putExtra(AppConstant.BUNDLE_PLACE_ADD, mDeatilInfo.getAddress());
                intent.putExtra(AppConstant.BUNDLE_PLACE_LAT, mDeatilInfo.getLocation().latitude);
                intent.putExtra(AppConstant.BUNDLE_PLACE_LNG, mDeatilInfo.getLocation().longitude);
                startActivity(intent);
                break;
            case R.id.iv_direction:
                String query = String.format("google.navigation:q=%s,%s", mDeatilInfo.getLocation().latitude, mDeatilInfo.getLocation().longitude);
                launchGoogleMapApp(query);
                break;
            case R.id.tv_more_review:
                String foursquareUrl = mDeatilInfo.getFoursquareUrl();
                if(!TextUtils.isEmpty(foursquareUrl))
                    openExternalBrowser(foursquareUrl);
                else
                    listener.showMessage("No url found");
                break;
            case R.id.rl_nearby:
                openVenueListScreen("Nearby Places");
                break;
            case R.id.rl_restaurants:
                openVenueListScreen("Restaurants");
                break;
            case R.id.tv_website:
                String website = mDeatilInfo.getWebsite();
                if(!TextUtils.isEmpty(website))
                    openExternalBrowser(website);
                else
                    listener.showMessage("No url found");
                break;
            case R.id.tv_google_maps:
                String geoUri = "http://maps.google.com/maps?q=loc:" + mDeatilInfo.getLocation().latitude + "," + mDeatilInfo.getLocation().longitude + " (" + mDeatilInfo.getPlaceName() + ")";
                launchGoogleMapApp(geoUri);
                break;
            case R.id.tv_foursquare:
                String url = mDeatilInfo.getFoursquareUrl();
                if(!TextUtils.isEmpty(url))
                    openExternalBrowser(url);
                else
                    listener.showMessage("No url found");
                break;
        }
    }

    private void openExternalBrowser(String url)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener= (VenueDetailFragmentListener)activity;
    }

    private void openVenueListScreen(String query)
    {
        Intent intent = new Intent(getActivity(), VenueListScreen.class);
        intent.putExtra(AppConstant.BUNDLE_PLACE_TYPE, query);
        startActivity(intent);
    }
}
