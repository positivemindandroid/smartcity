package com.positiveminds.smartcity.controller.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.SmartCityApplication;
import com.positiveminds.smartcity.controller.OperationManager;
import com.positiveminds.smartcity.controller.adapter.HomeAdapter;
import com.positiveminds.smartcity.model.ApplicationSettings;
import com.positiveminds.smartcity.model.Listener.DBOperationListener;
import com.positiveminds.smartcity.model.Listener.PlaceSearchOperationListener;
import com.positiveminds.smartcity.model.Listener.RecyclerItemClickListener;
import com.positiveminds.smartcity.model.Utils;
import com.positiveminds.smartcity.model.constant.AppConstant;
import com.positiveminds.smartcity.model.db.FavouritePlacesContract;
import com.positiveminds.smartcity.model.entity.Error;
import com.positiveminds.smartcity.model.entity.PlaceSearchInfo;
import com.positiveminds.smartcity.model.entity.SearchCriteria;
import com.positiveminds.smartcity.model.server.Url;
import com.positiveminds.smartcity.widget.HoloCircleSeekBar;
import com.positiveminds.smartcity.widget.RadiusSettingDialog;
import com.positiveminds.smartcity.widget.RateThisApp;
import com.positiveminds.smartcity.widget.SweetAlertDialog;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * Created by rajeev on 7/24/2015.
 */
public class HomeActivity extends BaseActivity implements PlaceSearchOperationListener, SearchView.OnQueryTextListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private static final int PORTRAIT_COLUMN_COUNT = 2;
    private static final int LANDSCAPE_COLUMN_COUNT = 3;
    private HomeAdapter mHomeAdapter;
    private List<String> mPlaceTypeList;
    private RecyclerView mRecyclerView;
    private String mPlaceType;
    private List<String> mFilteredModelList;
    private GoogleApiClient mGoogleApiClient;
    private static final String TAG = "User_Location";
    private boolean mUserSearch = false;
    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private int mDistanceType;
    private GoogleMap mGoogleMap;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        checkLocation();
        init();
        RateThisApp.init(new RateThisApp.Config(3, 5));
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initNavigationDrawer();
        mProgressBar = findViewById(R.id.bar_home_activity);
        mRecyclerView = (RecyclerView) findViewById(R.id.home_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            mRecyclerView.setLayoutManager(new GridLayoutManager(this, PORTRAIT_COLUMN_COUNT));
        } else {
            mRecyclerView.setLayoutManager(new GridLayoutManager(this, LANDSCAPE_COLUMN_COUNT));
        }
        String[] placeTypeArr = getResources().getStringArray(R.array.place_type_array);
        mPlaceTypeList = Arrays.asList(placeTypeArr);
        mFilteredModelList = mPlaceTypeList;
        mHomeAdapter = new HomeAdapter(mPlaceTypeList, HomeActivity.this);
        mRecyclerView.setAdapter(mHomeAdapter);
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(HomeActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        onClickPlaceType(position);
                    }
                })
        );
    }

    private void initNavigationDrawer() {
        navigationView = (NavigationView) findViewById(R.id.navigation_view);

        LayoutInflater inflater = getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.header, null, false);
        navigationView.addHeaderView(listHeaderView);
        handleNavigationHeaderView(listHeaderView);
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {


                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);

                //Closing drawer on item click
                drawerLayout.closeDrawers();

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.favourite:
                        showFavouritePlacesList();
                        return true;
                    case R.id.place_visit:
                        searchUserQuery("Tourist places");
                        return true;
                    default:
                        return true;
                }
            }
        });

        // Initializing Drawer Layout and ActionBarToggle
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank

                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessay or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }


    private void showFavouritePlacesList() {
        showProgress();
        OperationManager.getInstance().getVenuePlacesFromDB(new DBOperationListener() {
            @Override
            public void onSuccessInsertDB() {

            }

            @Override
            public void onSuccessUpdateDB() {

            }

            @Override
            public void onSuccessDeleteDB() {

            }

            @Override
            public void onSuccessReadDB(Cursor cursor) {
                if (cursor != null) {
                    LatLng currentLocation = SmartCityApplication.getAppInstance().getCurrentLocation();
                    ArrayList<PlaceSearchInfo> searchInfos = new ArrayList<PlaceSearchInfo>();
                    if (cursor.moveToFirst()) {
                        do {
                            String placeName = cursor.getString(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_NAME));
                            String placeId = cursor.getString(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_ID));
                            String placeAddress = cursor.getString(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_ADDRESS));
                            String lat = cursor.getString(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_LAT));
                            String lng = cursor.getString(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_LONG));
                            PlaceSearchInfo searchInfo = new PlaceSearchInfo();
                            searchInfo.setName(placeName);
                            searchInfo.setAddress(placeAddress);
                            searchInfo.setPlaceId(placeId);
                            if (currentLocation != null) {
                                float distance = Utils.distance(currentLocation.latitude, currentLocation.longitude, Double.valueOf(lat), Double.valueOf(lng));
                                searchInfo.setDistance(distance);
                            }
                            searchInfos.add(searchInfo);
                        }
                        while (cursor.moveToNext());
                    } else {
                        hideProgress();
                        showSnackBar("No Favourite Places Found");
                    }
                    cursor.close();
                    if (searchInfos.size() > 0) {
                        hideProgress();
                        SmartCityApplication.getAppInstance().setSearchInfoList(searchInfos);
                        launchSearchListScreen("Favourite Places");
                    }
                }
            }

            @Override
            public void onFailureDB(String errorMessage) {
                hideProgress();
                showSnackBar(errorMessage);
            }
        }, AppConstant.VENUE_FAVOURITE_TYPE);
    }


    private void handleNavigationHeaderView(View view) {
        setUpMapIfNeeded();
        mDistanceType = SmartCityApplication.getAppInstance().getDistanceType();
        TextView contentTextView = (TextView) view.findViewById(R.id.tv_content_text);
        contentTextView.setTypeface(SmartCityApplication.getAppInstance().getPrimaryTypeface());
        final HoloCircleSeekBar mSeekBar = (HoloCircleSeekBar) view.findViewById(R.id.seek_bar);
        view.findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int changedRadius = mSeekBar.getValue();
                if (changedRadius == 0)
                    changedRadius = 3;
                ApplicationSettings.putPref(AppConstant.PREF_DISTANCE_TYPE, mDistanceType);
                SmartCityApplication.getAppInstance().setDistanceType(mDistanceType);
                SmartCityApplication.getAppInstance().setRadius(changedRadius);
                ApplicationSettings.putPref(AppConstant.PREF_RADIUS, changedRadius);
                drawerLayout.closeDrawers();
            }
        });
        findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
            }
        });
        RadioButton kmRadion = (RadioButton) findViewById(R.id.radio_km);
        kmRadion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mDistanceType = AppConstant.KILOMETER_TYPE;
                    mSeekBar.setDistanceType(mDistanceType);
                }

            }
        });
        RadioButton mileRadio = (RadioButton) findViewById(R.id.radio_mile);
        mileRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mDistanceType = AppConstant.MILE_TYPE;
                    mSeekBar.setDistanceType(mDistanceType);
                }
            }
        });
        if (mDistanceType == AppConstant.KILOMETER_TYPE)
            kmRadion.setChecked(true);
        else
            mileRadio.setChecked(true);
    }

    private void setUpMapIfNeeded() {
        if (mGoogleMap == null) {
            mGoogleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_drawer)).getMap();
        }
        if (mGoogleMap != null) {
            LatLng location = SmartCityApplication.getAppInstance().getCurrentLocation();
            if (location != null) {
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.latitude, location.longitude), 14f));
            }
        }
    }


    private void onClickPlaceType(int position) {
        if (isInternetConnected()) {
            LatLng currentLocation = SmartCityApplication.getAppInstance().getCurrentLocation();
            if (currentLocation != null) {
                showProgress();
                mPlaceType = mFilteredModelList.get(position);
                if (!mUserSearch) {
                    SearchCriteria criteria = new SearchCriteria();
                    criteria.setPlaceType(mPlaceType);
                    int radius = SmartCityApplication.getAppInstance().getRadius();
                    if (SmartCityApplication.getAppInstance().getDistanceType() == AppConstant.KILOMETER_TYPE)
                        radius = radius * 1000;
                    else {
                        radius = radius * 1609;
                    }
                    criteria.setRadius(radius);
                    criteria.setLocation(currentLocation);
                    String url = Url.getPlaceSearchUrl(criteria);
                    url = Url.getFoursquarePlaceSearchUrl(mPlaceType, currentLocation,null);
                    OperationManager.getInstance().getSearchPlaceList(url, HomeActivity.this);
                } else {
                    searchUserQuery(mPlaceType);
                }
            } else
                showSnackBar("Current Location Not Found");
        } else
            showNoInternetDialog();

    }


    private void checkLocation() {
        if (!isLocationEnabled(this)) {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    openLocationDialog();

                }
            }, 1000);
        }
    }

    @Override
    public void onSuccessPlaceSearch(List<PlaceSearchInfo> searchInfoList) {
        hideProgress();
        if (searchInfoList != null && searchInfoList.size() > 0) {
            Collections.sort(searchInfoList, new PlaceSearchInfo());
            SmartCityApplication.getAppInstance().setSearchInfoList(searchInfoList);
            mPlaceType = mPlaceType.replace("_", " ");
            launchSearchListScreen(mPlaceType);
        }
    }

    private void launchSearchListScreen( String headerText) {
        Intent intent = new Intent(HomeActivity.this, SearchListActivity.class);
        intent.putExtra(AppConstant.BUNDLE_PLACE_TYPE, headerText);
        startActivity(intent);

    }

    private void filter(String query) {
        query = query.toLowerCase();

        mFilteredModelList = new ArrayList<>();
        for (String searchType : mPlaceTypeList) {
            if (searchType.toLowerCase(Locale.getDefault()).contains(query)) {
                mFilteredModelList.add(searchType);
                mUserSearch = false;
            }
        }
        if (mFilteredModelList.size() == 0) {
            mUserSearch = true;
            mFilteredModelList.add(query);
        }
        mHomeAdapter.animateTo(mFilteredModelList);
        mRecyclerView.scrollToPosition(0);
    }

    @Override
    public void onResume() {
        super.onResume();
        buildGoogleApiClient();
        setUpMapIfNeeded();
    }

    @Override
    public void onFailurePlaceSearch(com.positiveminds.smartcity.model.entity.Error error) {
        hideProgress();
        showSnackBar(error.getMessage());
        if (error.getErrorId() == Error.NO_LOCATION_FOUND) {
            openLocationDialog();
        } else if (error.getErrorId() == Error.NO_PLACE_FOUND) {
            showNoPlaceFoundAlert();
        }
    }


    private void showNoPlaceFoundAlert() {
        int radius = SmartCityApplication.getAppInstance().getRadius();
        String title = String.format("No Place Found with in %s km", radius);
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(title)
                .setContentText("Change search radius to get more places.").setCancelText("Not now!")
                .setConfirmText(getResources().getString(R.string.change_it_text)).showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();
                        showRadiusSettingPopUp();
                    }
                }).show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
        SearchView.SearchAutoComplete autoComplete = (SearchView.SearchAutoComplete) searchView.findViewById(R.id.search_src_text);
        autoComplete.setHint("Search your Place");
        autoComplete.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String query = v.getText().toString();
                    if (!TextUtils.isEmpty(query)) {
                        searchUserQuery(v.getText().toString());
                    } else {
                        showSnackBar("Empty query");
                    }
                    return true;
                }
                return false;
            }
        });
        return true;
    }


    private void searchUserQuery(String query) {
        try {
            LatLng currentLocation = SmartCityApplication.getAppInstance().getCurrentLocation();
            if (currentLocation != null) {
                mPlaceType = query;
                query = URLEncoder.encode(query, "UTF-8");
                String url = Url.getUserQueryUrl(query, currentLocation);
                showProgress();
                url = Url.getFoursquarePlaceSearchUrl(query,currentLocation,null);
                OperationManager.getInstance().getSearchPlaceList(url, HomeActivity.this);
            } else {
                showSnackBar("Location is off!");
                openLocationDialog();
            }
        } catch (UnsupportedEncodingException exception) {
            if (exception != null) {
                showSnackBar(exception.getMessage());
            }
        }

    }

    @Override
    public boolean onQueryTextChange(String query) {
        filter(query);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }


    @Override
    public void onConnected(Bundle bundle) {
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            SmartCityApplication.getAppInstance().setCurrentLocation(latLng);
            AddressTask addressTask = new AddressTask(mLastLocation);
            addressTask.execute();
        }
        else
        {
            showSnackBar("Enable your location for this app!", "Enable", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openLocationDialog();
                }
            });
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    buildGoogleApiClient();
                }
            },2000);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        if (i == CAUSE_SERVICE_DISCONNECTED) {
            Log.d(TAG, "Disconnected. Please re-connect.");
        } else if (i == CAUSE_NETWORK_LOST) {
            Log.d(TAG, "Network lost. Please re-connect.");
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        showSnackBar(connectionResult.toString());
    }


    private void showRadiusSettingPopUp() {
        RadiusSettingDialog dialog = new RadiusSettingDialog(HomeActivity.this);
        dialog.setRadiusDilaogBtnClickListener(new RadiusSettingDialog.OnRadiusDialogClickListener() {
            @Override
            public void onConfirmClick(RadiusSettingDialog dialog, int radius) {
                dialog.dismiss();
                SmartCityApplication.getAppInstance().setRadius(radius);
                ApplicationSettings.putPref(AppConstant.PREF_RADIUS, radius);
            }

            @Override
            public void onCancelClick(RadiusSettingDialog radiusSettingDialog) {
                radiusSettingDialog.dismiss();
            }
        });
        dialog.show();
    }

    public class AddressTask extends AsyncTask<Void, Void, List<Address>> {
        Location location;

        public AddressTask(Location location) {
            this.location = location;
        }


        @Override
        protected List<Address> doInBackground(Void... params) {
            try {
                Geocoder geocoder = new Geocoder(HomeActivity.this, Locale.getDefault());
                List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                return addresses;
            } catch (IOException ex) {
                if (ex != null && ex.getMessage() != null)
                    showSnackBar(ex.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<Address> addresses) {
            super.onPostExecute(addresses);
            if (addresses != null && addresses.size() > 0) {
                String city = addresses.get(0).getLocality();
                if (TextUtils.isEmpty(city))
                    city = "";
                String state = addresses.get(0).getAdminArea();
                if (TextUtils.isEmpty(state))
                    state = "";
                String zip = addresses.get(0).getPostalCode();
                if (TextUtils.isEmpty(zip))
                    zip = "";
                else
                    zip = String.format("(%s)", zip);
                String country = addresses.get(0).getCountryName();
                if (TextUtils.isEmpty(country))
                    country = "";
                String address = String.format("%s, %s, %s %s", city, state, country, zip);
                showSnackBar(address);
                setUpMapIfNeeded();
            }

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        RateThisApp.onStart(this);
        RateThisApp.showRateDialogIfNeeded(this);
    }
}
