package com.positiveminds.smartcity.controller.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.SmartCityApplication;
import com.positiveminds.smartcity.controller.OperationManager;
import com.positiveminds.smartcity.controller.adapter.ProductAdapter;
import com.positiveminds.smartcity.controller.adapter.SearchListAdapter;
import com.positiveminds.smartcity.model.Listener.DBOperationListener;
import com.positiveminds.smartcity.model.Listener.PlaceSearchOperationListener;
import com.positiveminds.smartcity.model.Listener.RecyclerItemClickListener;
import com.positiveminds.smartcity.model.constant.AppConstant;
import com.positiveminds.smartcity.model.db.FavouritePlacesContract;
import com.positiveminds.smartcity.model.entity.PlaceSearchInfo;
import com.positiveminds.smartcity.model.entity.Product;
import com.positiveminds.smartcity.model.server.Url;
import com.positiveminds.smartcity.widget.RateThisApp;
import com.positiveminds.smartcity.widget.RepeatableTransitionDrawable;
import com.positiveminds.smartcity.widget.TransitionDrawableExt;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Eric on 15/6/1.
 */
public class FoursquareHomeActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private RecyclerView recyclerView;
    private CollapsingToolbarLayout collapsingToolbar;
    private ImageView mHeaderImageView;
    private List<PlaceSearchInfo> mHistoryVenuesList;
    private static final String TAG = "User_Location";
    private GoogleApiClient mGoogleApiClient;
    private TextView mVenueListHeader;
    private boolean flag;
    private ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        initAdView(R.id.home_adView);
        imageLoader = ImageLoader.getInstance();
        checkLocation();
        RateThisApp.init(new RateThisApp.Config(3, 5));
        mProgressBar = findViewById(R.id.bar_recycle_activity);
        initToolbar();
        initViews();

    }

    private void checkLocation() {
        if (!isLocationEnabled(this)) {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    openLocationDialog();

                }
            }, 1000);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean result = super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.action_favourite:
                result = true;
                Intent intent = new Intent(FoursquareHomeActivity.this, VenueListScreen.class);
                intent.putExtra(AppConstant.BUNDLE_VENUE_SAVE_TYPE, AppConstant.VENUE_FAVOURITE_TYPE);
                startActivity(intent);
                break;
            case R.id.action_setting:
                result = true;
                startActivity(new Intent(FoursquareHomeActivity.this, SettingActivity.class));
                break;

        }
        return result;
    }

    private void initToolbar() {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setContentScrimColor(getResources().getColor(R.color.theme_color_transparent));
    }


    private void initViews() {
        mHeaderImageView = (ImageView)findViewById(R.id.iv_header);
        mVenueListHeader = (TextView)findViewById(R.id.tv_visit);
        adjustHeaderImageDimen();
        //startImageTransition();
        recyclerView = (RecyclerView) findViewById(R.id.home_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        EditText searchEditText = (EditText) findViewById(R.id.et_search_home);
        searchEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FoursquareHomeActivity.this, SearchActivity.class));
            }
        });
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(FoursquareHomeActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (isInternetConnected()) {
                            PlaceSearchInfo searchInfo = mHistoryVenuesList.get(position);
                            if (searchInfo != null) {
                                String placeId = searchInfo.getPlaceId();
                                if (placeId != null) {
                                    Intent intent = new Intent(FoursquareHomeActivity.this, VenueDetailActivity.class);
                                    SmartCityApplication.getAppInstance().setDetailsInfo(searchInfo);
                                    intent.putExtra(AppConstant.BUNDLE_PLACE_ID, placeId);
                                    intent.putExtra(AppConstant.BUNDLE_IMAGE_URL, searchInfo.getIconUrl());
                                    startActivity(intent);
                                }
                            }
                        } else
                            showNoInternetDialog();
                    }
                })
        );
        findViewById(R.id.tv_search_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mHistoryVenuesList != null && mHistoryVenuesList.size() > 0) {
                    for (PlaceSearchInfo info : mHistoryVenuesList) {
                        OperationManager.getInstance().clearHistoryListFromDB(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_ID, info.getPlaceId(), FavouritePlacesContract.PlaceEntry.TABLE_NAME);
                    }
                    mVenueListHeader.setText("Popular Nearby");
                    getPopularNearby();
                }
            }
        });
    }

    private void adjustHeaderImageDimen() {
        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth(); // ((display.getWidth()*20)/100)
        int height = (display.getHeight() * 55) / 100;// ((display.getHeight()*30)/100)
        LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(width, height);
        mHeaderImageView.setLayoutParams(parms);
    }

    private void getPopularNearby() {
        showProgress();
        LatLng currentLocation = SmartCityApplication.getAppInstance().getCurrentLocation();
        if(currentLocation != null) {
            String url = Url.getFoursquarePlaceSearchUrl("", currentLocation, "topPicks");
            OperationManager.getInstance().getSearchPlaceList(url, new PlaceSearchOperationListener() {
                @Override
                public void onSuccessPlaceSearch(List<PlaceSearchInfo> searchInfoList) {
                    mHistoryVenuesList = searchInfoList;
                    SearchListAdapter listAdapter = new SearchListAdapter(searchInfoList);
                    recyclerView.setAdapter(listAdapter);
                    hideProgress();
                    if(searchInfoList != null && searchInfoList.size() > 0)
                    {
                        List<String> urlList = new ArrayList<String>();
                        for(PlaceSearchInfo info : searchInfoList)
                        {
                            String url = info.getIconUrl();
                            if(!TextUtils.isEmpty(url))
                                urlList.add(url);
                        }
                        startImageTransition(urlList);
                    }
                }

                @Override
                public void onFailurePlaceSearch(com.positiveminds.smartcity.model.entity.Error error) {
                    hideProgress();
                }
            });
        }
    }


    private void getVenuePlacesFromDB(int venueType) {
        showProgress();
        OperationManager.getInstance().getVenuePlacesFromDB(new DBOperationListener() {
            @Override
            public void onSuccessInsertDB() {

            }

            @Override
            public void onSuccessUpdateDB() {

            }

            @Override
            public void onSuccessDeleteDB() {

            }

            @Override
            public void onSuccessReadDB(Cursor cursor) {
                hideProgress();
                if (cursor != null) {
                    ArrayList<PlaceSearchInfo> searchInfos = new ArrayList<PlaceSearchInfo>();
                    if (cursor.moveToFirst()) {
                        List<String >imageUrls = new ArrayList<String>();
                        mVenueListHeader.setText("Recent Searches");
                        do {
                            PlaceSearchInfo searchInfo = new PlaceSearchInfo();
                            String placeName = cursor.getString(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_NAME));
                            searchInfo.setName(placeName);
                            String placeId = cursor.getString(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_ID));
                            searchInfo.setPlaceId(placeId);
                            String placeAddress = cursor.getString(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_ADDRESS));
                            searchInfo.setAddress(placeAddress);
                            String distance = cursor.getString(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_DISTANCE));
                            searchInfo.setDistanceStr(distance);
                            int vote = cursor.getInt(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_VOTE));
                            searchInfo.setVote(vote);
                            String rating = cursor.getString(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_RATING));
                            searchInfo.setRating(rating);
                            String category = cursor.getString(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_CATEGORY));
                            searchInfo.setCategory(category);
                            String url = cursor.getString(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_URL));
                            searchInfo.setIconUrl(url);
                            imageUrls.add(url);
                            int venueSaveType = cursor.getInt(cursor.getColumnIndex(FavouritePlacesContract.PlaceEntry.COLUMN_NAME_VENUE_TYPE));
                            searchInfo.setVenueSaveType(venueSaveType);
                            searchInfos.add(searchInfo);
                        }
                        while (cursor.moveToNext());
                        startImageTransition(imageUrls);
                    } else {
                        mVenueListHeader.setText("Popular Nearby");
                        getPopularNearby();
                    }
                    cursor.close();
                    if (searchInfos.size() > 0) {

                        mHistoryVenuesList = searchInfos;
                        SearchListAdapter listAdapter = new SearchListAdapter(searchInfos);
                        recyclerView.setAdapter(listAdapter);
                        hideProgress();

                    }
                }
            }

            @Override
            public void onFailureDB(String errorMessage) {
                hideProgress();
                showSnackBar(errorMessage);
            }
        }, venueType);
    }

    private void startImageTransition(List<String> urls)
    {
        final List<Drawable> drawableList = new ArrayList<>();
        for(int i = 0 ; i< urls.size();i++)
        {
            String url = urls.get(i);
            if(!TextUtils.isEmpty(url))
            {
                imageLoader.loadImage(url, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String s, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String s, View view, FailReason failReason) {

                    }

                    @Override
                    public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                        if(bitmap != null)
                        {
                            drawableList.add(new BitmapDrawable(getResources(),bitmap));
                        }
                    }

                    @Override
                    public void onLoadingCancelled(String s, View view) {

                    }
                });
            }
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if( drawableList.size() >= 2)
                {
                    Drawable bgList[] = new Drawable[drawableList.size()];
                    drawableList.toArray(bgList);
                    TransitionDrawableExt transitionDrawable = new TransitionDrawableExt(bgList);
                    mHeaderImageView.setImageDrawable(transitionDrawable);
                    handlechange1(transitionDrawable);
                }
            }
        },2000);
    }

    void handlechange1(final TransitionDrawableExt transitionDrawable)
    {
        Handler hand = new Handler();
        hand.postDelayed(new Runnable() {
            @Override
            public void run() {
                change();
            }

            private void change() {
                if (flag) {
                    transitionDrawable.startTransition(3000);
                    flag = false;
                } else {
                    transitionDrawable.reverseTransition(3000);
                    flag = true;
                }
                handlechange1(transitionDrawable);
            }
        }, 3000);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onResume() {
        super.onResume();
        buildGoogleApiClient();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getVenuePlacesFromDB(AppConstant.VENUE_HISTORY_TYPE);
            }
        }, 2000);

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }


    @Override
    public void onConnected(Bundle bundle) {
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            SmartCityApplication.getAppInstance().setCurrentLocation(latLng);
            AddressTask addressTask = new AddressTask(mLastLocation);
            addressTask.execute();
        } else {
            showSnackBar("Enable your location for this app!", "Enable", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openLocationDialog();
                }
            });
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    buildGoogleApiClient();
                }
            }, 2000);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        if (i == CAUSE_SERVICE_DISCONNECTED) {
            Log.d(TAG, "Disconnected. Please re-connect.");
        } else if (i == CAUSE_NETWORK_LOST) {
            Log.d(TAG, "Network lost. Please re-connect.");
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        showSnackBar(connectionResult.toString());
    }

    public class AddressTask extends AsyncTask<Void, Void, List<Address>> {
        Location location;

        public AddressTask(Location location) {
            this.location = location;
        }


        @Override
        protected List<Address> doInBackground(Void... params) {
            try {
                Geocoder geocoder = new Geocoder(FoursquareHomeActivity.this, Locale.getDefault());
                List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                return addresses;
            } catch (IOException ex) {
                if (ex != null && ex.getMessage() != null)
                    showSnackBar(ex.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<Address> addresses) {
            super.onPostExecute(addresses);
            if (addresses != null && addresses.size() > 0) {
                String city = addresses.get(0).getLocality();
                if (TextUtils.isEmpty(city))
                    city = "";
                String state = addresses.get(0).getAdminArea();
                if (TextUtils.isEmpty(state))
                    state = "";
                String zip = addresses.get(0).getPostalCode();
                if (TextUtils.isEmpty(zip))
                    zip = "";
                else
                    zip = String.format("(%s)", zip);
                String country = addresses.get(0).getCountryName();
                if (TextUtils.isEmpty(country))
                    country = "";
                String address = String.format("%s, %s, %s %s", city, state, country, zip);
                showSnackBar(address);
            }

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        RateThisApp.onStart(this);
        RateThisApp.showRateDialogIfNeeded(this);
    }

    private void splitBitmap()
    {
        Bitmap[] bitmapsArray = new Bitmap[8];
        Bitmap bMap = BitmapFactory.decodeResource(getResources(), R.drawable.bg_home);
        Bitmap bMapScaled = Bitmap.createScaledBitmap(bMap, 240, 240, true);
        bitmapsArray[0] = Bitmap.createBitmap(bMapScaled, 0, 0, 80, 80);
        bitmapsArray[1] = Bitmap.createBitmap(bMapScaled, 80, 0, 80, 80);
        bitmapsArray[2] = Bitmap.createBitmap(bMapScaled, 160, 0, 80, 80);
        bitmapsArray[3] = Bitmap.createBitmap(bMapScaled, 0, 80, 80, 80);
        bitmapsArray[4] = Bitmap.createBitmap(bMapScaled, 80, 80, 80, 80);
        bitmapsArray[5] = Bitmap.createBitmap(bMapScaled, 160, 80, 80, 80);
        bitmapsArray[6] = Bitmap.createBitmap(bMapScaled, 0, 160, 80, 80);
        bitmapsArray[7] = Bitmap.createBitmap(bMapScaled, 80, 160, 80, 80);
        bitmapsArray[8] = Bitmap.createBitmap(bMapScaled, 160, 160, 80, 80);
    }


}
