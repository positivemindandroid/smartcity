package com.positiveminds.smartcity.controller.activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.model.constant.AppConstant;

/**
 * Created by RajeevTomar on 9/10/2015.
 */
public class VenueMapActivity extends BaseActivity {

    private GoogleMap mGoogleMap;
    private double lat ;
    private double lng ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue_map);
        initAdView(R.id.map_adView);
        initView();
        init();

    }

    private void initView()
    {
        findViewById(R.id.iv_map_direction).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String query = String.format("google.navigation:q=%s,%s", lat, lng);
                launchGoogleMapApp(query);
            }
        });
    }

    private void init()
    {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUpMapIfNeeded();
        if (getIntent() != null) {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                String name = bundle.getString(AppConstant.BUNDLE_PLACE_NAME);
                String title = bundle.getString(AppConstant.BUNDLE_PLACE_ADD);
                lat = bundle.getDouble(AppConstant.BUNDLE_PLACE_LAT);
                lng = bundle.getDouble(AppConstant.BUNDLE_PLACE_LNG);
                if(mGoogleMap != null)
                {
                        Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, lng))
                                .title(name).snippet(title));
                        marker.showInfoWindow();
                        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 16f));
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            showInterstitialAdd();
                        }
                    }, 2000);
                }
            }
        }
    }

    private void setUpMapIfNeeded() {
        if (mGoogleMap != null) {
            return;
        }
        mGoogleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_venue)).getMap();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
