package com.positiveminds.smartcity.controller.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.SmartCityApplication;
import com.positiveminds.smartcity.model.ApplicationSettings;
import com.positiveminds.smartcity.model.constant.AppConstant;

/**
 * Created by RajeevTomar on 9/4/2015.
 */
public class SettingActivity extends  BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        initView();
    }

    private void initView()
    {
        int distanceType = SmartCityApplication.getAppInstance().getDistanceType();
        RadioButton mileRadio = (RadioButton) findViewById(R.id.radio_mile);
        RadioButton kmRadion = (RadioButton) findViewById(R.id.radio_km);
        if (distanceType == AppConstant.KILOMETER_TYPE)
            kmRadion.setChecked(true);
        else
            mileRadio.setChecked(true);

        kmRadion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SmartCityApplication.getAppInstance().setDistanceType(AppConstant.KILOMETER_TYPE);
                    ApplicationSettings.putPref(AppConstant.PREF_DISTANCE_TYPE, AppConstant.KILOMETER_TYPE);
                }

            }
        });
        mileRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SmartCityApplication.getAppInstance().setDistanceType(AppConstant.MILE_TYPE);
                    ApplicationSettings.putPref(AppConstant.PREF_DISTANCE_TYPE, AppConstant.MILE_TYPE);

                }
            }
        });
        findViewById(R.id.tv_rate_us).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try
                {
                    Intent intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("market://details?id=" + getPackageName()));
                    startActivity(intent);
                }
                catch(ActivityNotFoundException ex)
                {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName()));
                    startActivity(intent);
                }
            }
        });

        findViewById(R.id.tv_share_us).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                String text = String.format("Check it out! It's awesome. \n https://play.google.com/store/apps/details?id=%s",getPackageName());
                shareIntent.putExtra(Intent.EXTRA_TEXT, text);
                shareIntent.setType("text/plain");
                startActivity(Intent.createChooser(shareIntent, "Share app link to.."));
            }
        });
    }

}
