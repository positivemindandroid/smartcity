package com.positiveminds.smartcity.controller;

import android.location.Criteria;

import com.google.android.gms.maps.model.LatLng;
import com.positiveminds.smartcity.model.Listener.DBOperationListener;
import com.positiveminds.smartcity.model.Listener.PlaceDetailsOperationListener;
import com.positiveminds.smartcity.model.Listener.PlaceSearchOperationListener;
import com.positiveminds.smartcity.model.Listener.SuggestionOperationListener;
import com.positiveminds.smartcity.model.constant.AppConstant;
import com.positiveminds.smartcity.model.db.FavouritePlacesContract;
import com.positiveminds.smartcity.model.entity.SearchCriteria;
import com.positiveminds.smartcity.model.operation.PlaceSearchOperation;
import com.positiveminds.smartcity.model.operation.PlaceDetailsOperation;
import com.positiveminds.smartcity.model.operation.PlaceSuggestionOperaiton;

/**
 * Created by rajeev on 7/24/2015.
 */
public class OperationManager {

    private static OperationManager mInstance;

    public static OperationManager getInstance()
    {
        if(mInstance == null){
            mInstance = new OperationManager();
        }
        return mInstance;
    }

    public void getSearchPlaceList(String  url, PlaceSearchOperationListener listener)
    {
        new PlaceSearchOperation().getSearchPlaceList(url, listener);
    }

    public void getPlaceDetailsList(String url,PlaceDetailsOperationListener listener)
    {
        new PlaceDetailsOperation().getPlaceDetails(url, listener);
    }

    public void getVenuePlacesFromDB(DBOperationListener listener, int venueType)
    {
        String selection = String.format("%s=?",FavouritePlacesContract.PlaceEntry.COLUMN_NAME_VENUE_TYPE);
        String[] selectionArgs = new String[]{String.valueOf(venueType)};
        String sortOrder = String.format("%s DESC", FavouritePlacesContract.PlaceEntry._ID);
        DBManager.getInstance().readDB(FavouritePlacesContract.PlaceEntry.TABLE_NAME, null, selection, selectionArgs, sortOrder, listener);
    }

    public void getAutoSuggestionList(String query, LatLng location, SuggestionOperationListener listener)
    {
        new PlaceSuggestionOperaiton().getAutoSuggestionPlaces(query, location, listener);
    }

    public void clearHistoryListFromDB(String key, String value, String tableName)
    {
        DBManager.getInstance().delete(key, value, tableName);
    }

}
