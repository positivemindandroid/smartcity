package com.positiveminds.smartcity.controller.adapter;

import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.location.places.Place;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.SmartCityApplication;
import com.positiveminds.smartcity.model.Utils;
import com.positiveminds.smartcity.model.constant.AppConstant;
import com.positiveminds.smartcity.model.entity.PlaceSearchInfo;

import java.util.List;

/**
 * Created by RajeevTomar on 7/27/2015.
 */
public class SearchListAdapter extends RecyclerView.Adapter<SearchListAdapter.ViewHolder> {

    private List<PlaceSearchInfo> mSearchInfoList;
    private int distanceType = SmartCityApplication.getAppInstance().getDistanceType();
    private String distanceStr;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private ImageSize imageSize;

    public SearchListAdapter(List<PlaceSearchInfo> searchInfoList) {
        mSearchInfoList = searchInfoList;
        imageLoader = ImageLoader.getInstance();
        if (distanceType == AppConstant.KILOMETER_TYPE)
            distanceStr = AppConstant.KILOMETER;
        else
            distanceStr = AppConstant.MILE;
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        imageSize = new ImageSize(100, 100);
    }

    @Override
    public SearchListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_search_list, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(SearchListAdapter.ViewHolder holder, int position) {
        PlaceSearchInfo info = mSearchInfoList.get(position);
        if (info != null) {
            holder.mTextView.setText(info.getName());
            String city = info.getCityName();
            if (!TextUtils.isEmpty(city)) {
                String add = info.getAddress();
                if (TextUtils.isEmpty(add))
                    add = "";
                String address = String.format("%s, %s", add, city);
                holder.addressTextView.setText(address);
            }
            else if(!TextUtils.isEmpty(info.getAddress()))
            {
                holder.addressTextView.setText(info.getAddress());
            }
            if (info.getDistance() > 0) {
                String distance = String.format("%s %s", info.getDistance(), distanceStr);
                holder.distanceTextView.setText(distance);
            }
            else if(!TextUtils.isEmpty(info.getDistanceStr()))
            {
                holder.distanceTextView.setText(info.getDistanceStr());
            }
            String category = info.getCategory();
            if (!TextUtils.isEmpty(category))
                holder.categoryTextView.setText(category);
            int vote = info.getVote();
            if (vote > 0) {
                holder.voteTextView.setVisibility(View.VISIBLE);
                String votes = String.format("%s votes", vote);
                holder.voteTextView.setText(votes);
            } else
                holder.voteTextView.setVisibility(View.INVISIBLE);
            float rating = info.getRatingInt();
            if (rating > 0) {
                holder.ratingTextView.setVisibility(View.VISIBLE);
                holder.ratingTextView.setText(String.valueOf(rating));
            }
            else if(!TextUtils.isEmpty(info.getRating())) {
                holder.ratingTextView.setVisibility(View.VISIBLE);
                holder.ratingTextView.setText(info.getRating());

            }else
                holder.ratingTextView.setVisibility(View.INVISIBLE);
            holder.venueImageView.setImageDrawable(null);
            String imageUrl = info.getIconUrl();
            if (!TextUtils.isEmpty(imageUrl)) {
                imageLoader.displayImage(imageUrl, holder.venueImageView, options);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mSearchInfoList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;
        public TextView addressTextView;
        public TextView distanceTextView;
        public TextView ratingTextView;
        public TextView voteTextView;
        public TextView categoryTextView;
        public ImageView venueImageView;

        public ViewHolder(View v) {
            super(v);
            Typeface myTypeface = SmartCityApplication.getAppInstance().getPrimaryTypeface();
            mTextView = (TextView) v.findViewById(R.id.tv_place_name);
            addressTextView = (TextView) v.findViewById(R.id.tv_place_address);
            distanceTextView = (TextView) v.findViewById(R.id.tv_distance);
            venueImageView = (ImageView) v.findViewById(R.id.iv_venue);
            ratingTextView = (TextView) v.findViewById(R.id.tv_rating);
            voteTextView = (TextView) v.findViewById(R.id.tv_vote);
            categoryTextView = (TextView) v.findViewById(R.id.tv_category);
            mTextView.setTypeface(myTypeface);
            //distanceTextView.setTypeface(myTypeface);
            //addressTextView.setTypeface(myTypeface);
        }

    }

    public void loadData(List<PlaceSearchInfo> infoList) {
        this.mSearchInfoList = infoList;
        notifyDataSetChanged();
    }

}
