package com.positiveminds.smartcity.model.operation;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.positiveminds.smartcity.SmartCityApplication;
import com.positiveminds.smartcity.model.Listener.PlaceSearchOperationListener;
import com.positiveminds.smartcity.model.entity.Error;
import com.positiveminds.smartcity.model.task.FourSquareJsonParser;
import com.positiveminds.smartcity.model.task.GoogleJsonParser;

import org.json.JSONObject;

/**
 * Created by RajeevTomar on 7/24/2015.
 */
public class PlaceSearchOperation {

    public void getSearchPlaceList(String  url, final PlaceSearchOperationListener listener) {
       JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null)
                    FourSquareJsonParser.parseVenueExploreJson(response, listener);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null)
                {
                    String message = "Server Error! Please try after some time";
                    if(error.getMessage() != null)
                        message = error.getMessage();
                    listener.onFailurePlaceSearch(new com.positiveminds.smartcity.model.entity.Error(Error.DEFAULT_CONNECTION_ERROR, message));
                }
            }
        });
        SmartCityApplication.getAppInstance().getRequestQueue().add(request);
    }


}
