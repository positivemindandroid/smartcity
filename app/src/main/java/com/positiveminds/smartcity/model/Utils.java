package com.positiveminds.smartcity.model;

import com.positiveminds.smartcity.SmartCityApplication;
import com.positiveminds.smartcity.model.constant.AppConstant;

import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by RajeevTomar on 7/27/2015.
 */
public class Utils {

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }


    public static float distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        int distanceType = SmartCityApplication.getAppInstance().getDistanceType();
        if(distanceType == AppConstant.KILOMETER_TYPE)
        {
            dist = dist * 1.609344;
        }
        NumberFormat formatter = new DecimalFormat("#0.00");
        String result = formatter.format(dist);
        return Float.valueOf(result);
    }

    public static double meterToKMI(int meter,int unitType)
    {
        double dist = 0;
        if(unitType == AppConstant.KILOMETER_TYPE)
        {
            dist= meter*.001;
        }
        else if( unitType == AppConstant.MILE_TYPE)
        {
            dist=  meter/1609.34;
        }
        NumberFormat formatter = new DecimalFormat("#0.00");
        String result = formatter.format(dist);
        return Double.valueOf(result);
    }


    private static double deg2rad(double deg) {

        return (deg * Math.PI / 180.0);

    }

    private static double rad2deg(double rad) {

        return (rad * 180 / Math.PI);

    }


}
