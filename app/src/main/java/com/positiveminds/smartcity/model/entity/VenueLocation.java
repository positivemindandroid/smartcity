package com.positiveminds.smartcity.model.entity;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by RajeevTomar on 9/9/2015.
 */
public class VenueLocation implements ClusterItem {
    private final LatLng mPosition;
    private final String title;

    public VenueLocation(double lat, double lng, String title) {
        mPosition = new LatLng(lat, lng);
        this.title  =title;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    public String getTitle() {
        return title;
    }
}