package com.positiveminds.smartcity.model.Listener;

import com.positiveminds.smartcity.model.entity.*;

import java.util.List;

/**
 * Created by RajeevTomar on 8/5/2015.
 */
public interface PlaceDetailsOperationListener {

    public void onSuccessGetDetails(PlaceDetailsInfo detailsInfos);
    public void onFailureGetDetails(com.positiveminds.smartcity.model.entity.Error error);
}
