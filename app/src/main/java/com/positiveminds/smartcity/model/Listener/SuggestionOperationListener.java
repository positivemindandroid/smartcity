package com.positiveminds.smartcity.model.Listener;

import com.positiveminds.smartcity.model.entity.*;

import java.util.List;

/**
 * Created by RajeevTomar on 9/14/2015.
 */
public interface SuggestionOperationListener {

    public void onSuccessfullGetSuggestion(List<AutoSuggestion> autoSuggestions);
    public void onFailureGetSuggestion(com.positiveminds.smartcity.model.entity.Error error);

}
