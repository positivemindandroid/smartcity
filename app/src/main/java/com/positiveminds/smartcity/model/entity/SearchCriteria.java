package com.positiveminds.smartcity.model.entity;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by RajeevTomar on 7/24/2015.
 */
public class SearchCriteria {

    private String placeType;
    private LatLng location;
    private int radius;

    public String getPlaceType() {
        return placeType;
    }

    public void setPlaceType(String placeType) {
        this.placeType = placeType;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }
}
