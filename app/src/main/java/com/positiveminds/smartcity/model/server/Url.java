package com.positiveminds.smartcity.model.server;

import android.text.TextUtils;

import com.google.android.gms.maps.model.LatLng;
import com.positiveminds.smartcity.SmartCityApplication;
import com.positiveminds.smartcity.model.constant.AppConstant;
import com.positiveminds.smartcity.model.entity.SearchCriteria;

import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by RajeevTomar on 7/24/2015.
 */
public class Url {


    private static final String GOOGLE_MAIN_URL = "https://maps.googleapis.com/maps/api/place/";
    private static final String FOURSQUARE_MAIN_URL = "https://api.foursquare.com/v2/venues/";
    private static final String NEAR_BY_SEARCH_URL = String.format("%snearbysearch/json?", GOOGLE_MAIN_URL);
    private static final String PLACE_DETAILS_URL = String.format("%sdetails/json?placeid=", GOOGLE_MAIN_URL);
    private static final String USER_QUERY_URL = String.format("%stextsearch/json?", GOOGLE_MAIN_URL);
    private static final String FOURSQUARE_COMMON_API=String.format("?client_id=%s&client_secret=%s&v=20130815",AppConstant.FOURSQUARE_CLIENT_ID, AppConstant.FOURSQUARE_CLIENT_SECRET);
    private static final String FOURSQUARE_PLACE_SEARCH_URL = String.format("%sexplore%s&", FOURSQUARE_MAIN_URL, FOURSQUARE_COMMON_API);

    public static String getPlaceSearchUrl(SearchCriteria criteria) {
        try {
            String placeType = URLEncoder.encode(criteria.getPlaceType(), "UTF-8");
            return String.format("%slocation=%s,%s&radius=%s&types=%s&key=%s", NEAR_BY_SEARCH_URL, criteria.getLocation().latitude, criteria.getLocation().longitude, criteria.getRadius(), placeType, AppConstant.GOOGLE_API_KEY);
        } catch (UnsupportedEncodingException ex) {
            return null;
        }
    }

    public static String getPlaceDetailUrl(String placeId) {
        return String.format("%s%s&key=%s", PLACE_DETAILS_URL, placeId, AppConstant.GOOGLE_API_KEY);
    }

    public static String getUserQueryUrl(String query, LatLng currentLocation) {
        int radius = SmartCityApplication.getAppInstance().getRadius() * 1000;
        return String.format("%slocation=%s,%s&radius=%s&query=%s&key=%s", USER_QUERY_URL, currentLocation.latitude, currentLocation.longitude, radius, query, AppConstant.GOOGLE_API_KEY);
    }

    public static String getFoursquarePlaceSearchUrl(String query, LatLng currentLocation, String selection) {
        String url =   String.format("%sll=%s,%s&venuePhotos=1&sortByDistance=1",FOURSQUARE_PLACE_SEARCH_URL,currentLocation.latitude,currentLocation.longitude);
        if(!TextUtils.isEmpty(query))
        {
            url = String.format("%s&query=%s",url,query);
        }
        if(!TextUtils.isEmpty(selection)){
            url = String.format("%s&selection=%s",url,selection);
        }
        return url;
    }

    public static String getFoursquareVenueDetailUrl(String venueId)
    {
        //https://api.foursquare.com/v2/venues/40a55d80f964a52020f31ee3?oauth_token=TY3D2ITDFUXI1UL1151CJYCD1V1BULKDJTCEOUYDWCIN2KNL&v=20150904
        return String.format("%s%s%s",FOURSQUARE_MAIN_URL,venueId,FOURSQUARE_COMMON_API);
    }


    public static String getFoursquareSuggestionUrl(String query, LatLng currentLocation)
    {
        //https://api.foursquare.com/v2/venues/suggestCompletion?ll=40.7,-74&query=spor&oauth_token=TY3D2ITDFUXI1UL1151CJYCD1V1BULKDJTCEOUYDWCIN2KNL&v=20150914
        return String.format("%ssuggestCompletion%s&ll=%s,%s&query=%s&limit=10",FOURSQUARE_MAIN_URL,FOURSQUARE_COMMON_API,currentLocation.latitude,currentLocation.longitude,query);
    }


    public static String getFoursquarePhotoUrl(String venueId) {
        return String.format("%s/%s/photos%s",FOURSQUARE_MAIN_URL,venueId,FOURSQUARE_COMMON_API);
    }




}
