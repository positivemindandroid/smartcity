package com.positiveminds.smartcity.model.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.positiveminds.smartcity.model.db.FavouritePlacesContract;

/**
 * Created by RajeevTomar on 8/19/2015.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String INTEGER_TYPE=" INTEGER";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + FavouritePlacesContract.PlaceEntry.TABLE_NAME + " (" +
                    FavouritePlacesContract.PlaceEntry._ID + " INTEGER PRIMARY KEY," +
                    FavouritePlacesContract.PlaceEntry.COLUMN_NAME_VOTE + INTEGER_TYPE + COMMA_SEP +
                    FavouritePlacesContract.PlaceEntry.COLUMN_NAME_VENUE_TYPE + INTEGER_TYPE + COMMA_SEP +
                    FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_NAME + TEXT_TYPE + COMMA_SEP +
                    FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_ID + TEXT_TYPE + COMMA_SEP +
                    FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_LAT + TEXT_TYPE + COMMA_SEP +
                    FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_LONG + TEXT_TYPE + COMMA_SEP +
                    FavouritePlacesContract.PlaceEntry.COLUMN_NAME_CATEGORY + TEXT_TYPE + COMMA_SEP +
                    FavouritePlacesContract.PlaceEntry.COLUMN_NAME_DISTANCE + TEXT_TYPE + COMMA_SEP +
                    FavouritePlacesContract.PlaceEntry.COLUMN_NAME_RATING + TEXT_TYPE + COMMA_SEP +
                    FavouritePlacesContract.PlaceEntry.COLUMN_NAME_URL + TEXT_TYPE + COMMA_SEP +
                    FavouritePlacesContract.PlaceEntry.COLUMN_NAME_PLACE_ADDRESS + TEXT_TYPE + " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + FavouritePlacesContract.PlaceEntry.TABLE_NAME;

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "SmartCity.db";

    public DBHelper(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.e("TABLE", SQL_CREATE_ENTRIES);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }


}
