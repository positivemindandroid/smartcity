package com.positiveminds.smartcity.model.operation;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.model.LatLng;
import com.positiveminds.smartcity.SmartCityApplication;
import com.positiveminds.smartcity.model.Listener.SuggestionOperationListener;
import com.positiveminds.smartcity.model.entity.*;
import com.positiveminds.smartcity.model.server.Url;
import com.positiveminds.smartcity.model.task.FourSquareJsonParser;

import org.json.JSONObject;

/**
 * Created by RajeevTomar on 9/14/2015.
 */
public class PlaceSuggestionOperaiton {

    public void getAutoSuggestionPlaces(String query, LatLng location, final SuggestionOperationListener listener)
    {
        String url = Url.getFoursquareSuggestionUrl(query,location);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null)
                    FourSquareJsonParser.parseAutoSuggestionList(response, listener);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null)
                {
                    String message = "Server Error! Please try after some time";
                    if(error.getMessage() != null)
                        message = error.getMessage();
                    listener.onFailureGetSuggestion(new com.positiveminds.smartcity.model.entity.Error(com.positiveminds.smartcity.model.entity.Error.DEFAULT_CONNECTION_ERROR, message));
                }
            }
        });
        SmartCityApplication.getAppInstance().getRequestQueue().add(request);
    }
}
