package com.positiveminds.smartcity.model.entity;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class Person implements ClusterItem {
    public final String name;
    public final String profilePhoto;
    private final LatLng mPosition;
    private final int itemPosition;
    private final String address;

    public Person(LatLng position, String name, String pictureResource, int itemPosition, String address) {
        this.name = name;
        profilePhoto = pictureResource;
        mPosition = position;
        this.itemPosition = itemPosition;
        this.address = address;
    }

    public int getItemPosition() {
        return itemPosition;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public String getAddress() {
        return address;
    }
}
