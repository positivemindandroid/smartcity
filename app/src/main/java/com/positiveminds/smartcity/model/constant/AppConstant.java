package com.positiveminds.smartcity.model.constant;

/**
 * Created by RajeevTomar on 7/24/2015.
 */
public class AppConstant {

    //KEYS
    public static final String GOOGLE_API_KEY="AIzaSyBXg1pmOopJPvwZCUm7R7aWhGD9CsV7dBs";
    public static final String FONT_PATH = "fonts/ArchivoNarrow.otf";

    public static final String FOURSQUARE_CLIENT_ID = "MDXA5K5G0SUJWQJCNSFJPFOKXO4SW00THPKMQTXL1UTFHREU";
    public static final String FOURSQUARE_CLIENT_SECRET = "2QRC3I2U5LNTBCXMYZP1IFPYKABD0H3Q0QIYYGFXLKHYBRZI";


    public static final int RADIUS= 5;  // Km
    public static final String BUNDLE_PLACE_ID = "place_id";
    public static final String BUNDLE_PLACE_TYPE = "place_type";
    public static final String BUNDLE_DISTANCE="place_distance";
    public static final String BUNDLE_PLACE_NAME ="place_name";
    public static final String BUNDLE_PLACE_ADD ="place_add";
    public static final String BUNDLE_PLACE_LAT ="place_lat";
    public static final String BUNDLE_PLACE_LNG ="place_lng";
    public static final String BUNDLE_IMAGE_URL ="image_url";
    public static final String BUNDLE_VENUE_SAVE_TYPE="venue_save_type";


    //PREF
    public static final String PREF_RADIUS = "pref_radius";
    public static final String PREF_DISTANCE_TYPE ="pref_distance_type";

    public static final String KILOMETER = "km";
    public static final String MILE="mile";
    public static final int KILOMETER_TYPE = 101;
    public static final int MILE_TYPE=102;


    public static final int VENUE_FAVOURITE_TYPE = 1001;
    public static final int VENUE_HISTORY_TYPE = 1002;


}
