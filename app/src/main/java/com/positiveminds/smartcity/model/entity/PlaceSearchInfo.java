package com.positiveminds.smartcity.model.entity;

import com.google.android.gms.maps.model.LatLng;

import java.util.Comparator;

/**
 * Created by RajeevTomar on 7/24/2015.
 */
public class PlaceSearchInfo implements Comparator<PlaceSearchInfo> {

    private String name;
    private LatLng location;
    private String placeId;
    private String address;
    private boolean openNow;
    private String rating;
    private String iconUrl;
    private double distance;
    private String url;
    private String contactNumber;
    private String cityName;
    private float ratingInt;
    private int vote;
    private String category;
    private String distanceStr;
    private int venueSaveType;




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isOpenNow() {
        return openNow;
    }

    public void setOpenNow(boolean openNow) {
        this.openNow = openNow;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }


    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public float getRatingInt() {
        return ratingInt;
    }

    public void setRatingInt(float ratingInt) {
        this.ratingInt = ratingInt;
    }

    public int getVote() {
        return vote;
    }

    public void setVote(int vote) {
        this.vote = vote;
    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDistanceStr() {
        return distanceStr;
    }

    public void setDistanceStr(String distanceStr) {
        this.distanceStr = distanceStr;
    }

    @Override
    public int compare(PlaceSearchInfo lhs, PlaceSearchInfo rhs) {
        if (lhs.getDistance() > rhs.getDistance()) {
            return 1;
        } else {
            return -1;
        }
    }

    public int getVenueSaveType() {
        return venueSaveType;
    }

    public void setVenueSaveType(int venueSaveType) {
        this.venueSaveType = venueSaveType;
    }
}
