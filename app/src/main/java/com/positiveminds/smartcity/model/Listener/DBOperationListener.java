package com.positiveminds.smartcity.model.Listener;

import android.database.Cursor;

/**
 * Created by RajeevTomar on 8/19/2015.
 */
public interface DBOperationListener {

    public void onSuccessInsertDB();
    public void onSuccessUpdateDB();
    public void onSuccessDeleteDB();
    public void onSuccessReadDB(Cursor cursor);
    public void onFailureDB(String errorMessage);
}
