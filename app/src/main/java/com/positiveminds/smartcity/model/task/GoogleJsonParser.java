package com.positiveminds.smartcity.model.task;

import com.google.android.gms.maps.model.LatLng;
import com.positiveminds.smartcity.SmartCityApplication;
import com.positiveminds.smartcity.model.Listener.PlaceDetailsOperationListener;
import com.positiveminds.smartcity.model.Listener.PlaceSearchOperationListener;
import com.positiveminds.smartcity.model.Utils;
import com.positiveminds.smartcity.model.entity.*;
import com.positiveminds.smartcity.model.entity.Error;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RajeevTomar on 7/24/2015.
 */
public class GoogleJsonParser {
    public static void handleSearchPlaceList(JSONObject response, PlaceSearchOperationListener listener) {
        try {
            JSONArray resultsArr = response.getJSONArray("results");
            if (resultsArr != null && resultsArr.length() > 0) {
                LatLng currentLocation = SmartCityApplication.getAppInstance().getCurrentLocation();
                if (currentLocation != null) {
                    double lat1 = currentLocation.latitude;
                    double long1 = currentLocation.longitude;

                    List<PlaceSearchInfo> placeSearchInfoList = new ArrayList<>();
                    for (int i = 0; i < resultsArr.length(); i++) {
                        JSONObject placeInfoObj = resultsArr.getJSONObject(i);
                        if (placeInfoObj != null) {
                            PlaceSearchInfo searchInfo = new PlaceSearchInfo();
                            if (placeInfoObj.has("geometry")) {
                                JSONObject geoObj = placeInfoObj.getJSONObject("geometry");
                                if (geoObj != null && geoObj.has("location")) {
                                    JSONObject locObj = geoObj.getJSONObject("location");
                                    if (locObj != null) {

                                        double lat2 = locObj.getDouble("lat");
                                        double long2 = locObj.getDouble("lng");
                                        float distance = Utils.distance(lat1, long1, lat2, long2);
                                        searchInfo.setDistance(distance);
                                        LatLng latLng = new LatLng(lat2, long2);
                                        searchInfo.setLocation(latLng);
                                    }
                                }
                            }
                            if (placeInfoObj.has("name")) {
                                String name = placeInfoObj.getString("name");
                                searchInfo.setName(name);
                            }
                            if (placeInfoObj.has("opening_hours")) {
                                JSONObject openHourObj = placeInfoObj.getJSONObject("opening_hours");
                                if (openHourObj.has("open_now")) {
                                    boolean openNow = openHourObj.getBoolean("open_now");
                                    searchInfo.setOpenNow(openNow);
                                }
                            }
                            String placeId = placeInfoObj.getString("place_id");
                            searchInfo.setPlaceId(placeId);
                            if (placeInfoObj.has("rating")) {
                                String rating = placeInfoObj.getString("rating");
                                searchInfo.setRating(rating);
                            }
                            if (placeInfoObj.has("vicinity")) {
                                String address = placeInfoObj.getString("vicinity");
                                searchInfo.setAddress(address);
                            }
                            else if( placeInfoObj.has("formatted_address"))
                            {
                                String address = placeInfoObj.getString("formatted_address");
                                searchInfo.setAddress(address);
                            }
                            placeSearchInfoList.add(searchInfo);
                        }
                    }
                    listener.onSuccessPlaceSearch(placeSearchInfoList);
                }
                else
                {
                    listener.onFailurePlaceSearch(new Error(Error.NO_LOCATION_FOUND,"No Location Found"));
                }
            }
            else {
                com.positiveminds.smartcity.model.entity.Error error = new Error();
                error.setErrorId(Error.NO_PLACE_FOUND);
                error.setMessage("No Place Found");
                if (response.has("error_message"))
                {
                    error.setErrorId(Error.DEFAULT_CONNECTION_ERROR);
                    error.setMessage(response.getString("error_message"));
                }
                listener.onFailurePlaceSearch(error);
            }

        } catch (JSONException exception) {
            listener.onFailurePlaceSearch(new Error(Error.DEFAULT_CONNECTION_ERROR,exception.getMessage()));
        }
    }

    public static void parsePlaceDetails(JSONObject response, PlaceDetailsOperationListener listener) {
        try {
            JSONObject resultsObj = response.getJSONObject("result");
            if (resultsObj != null) {
                PlaceDetailsInfo detailsInfo = new PlaceDetailsInfo();
                if (resultsObj.has("formatted_address")) {
                    String address = resultsObj.getString("formatted_address");
                    if (address != null)
                        detailsInfo.setAddress(address);
                }
                if (resultsObj.has("formatted_phone_number")) {
                    String phoneNumber = resultsObj.getString("formatted_phone_number");
                    if (phoneNumber != null)
                        detailsInfo.setPhoneNumber(phoneNumber);
                }
                if (resultsObj.has("geometry")) {
                    JSONObject geoObj = resultsObj.getJSONObject("geometry");
                    if (geoObj != null && geoObj.has("location")) {
                        JSONObject locObj = geoObj.getJSONObject("location");
                        if (locObj != null) {
                            LatLng latLng = new LatLng(locObj.getDouble("lat"), locObj.getDouble("lng"));
                            detailsInfo.setLocation(latLng);
                        }
                    }
                }
                if (resultsObj.has("name")) {
                    String name = resultsObj.getString("name");
                    detailsInfo.setPlaceName(name);
                }
                if (resultsObj.has("rating")) {
                    double rating = resultsObj.getDouble("rating");
                    detailsInfo.setRating(rating);
                }
                if (resultsObj.has("url")) {
                    String mapUrl = resultsObj.getString("url");
                    detailsInfo.setMapUrl(mapUrl);
                }
                if (resultsObj.has("website")) {
                    String website = resultsObj.getString("website");
                    detailsInfo.setWebsite(website);
                }
                listener.onSuccessGetDetails(detailsInfo);
            }
        } catch (JSONException exception) {
            listener.onFailureGetDetails(new Error(Error.DEFAULT_CONNECTION_ERROR,exception.getMessage()));
        }
    }

}
