package com.positiveminds.smartcity.model.entity;

/**
 * Created by RajeevTomar on 9/14/2015.
 */
public class AutoSuggestion {

    private  String name;
    private  String address;

    public AutoSuggestion()
    {

    }

    public AutoSuggestion(String name, String address)
    {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
