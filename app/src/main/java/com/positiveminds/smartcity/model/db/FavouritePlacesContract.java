package com.positiveminds.smartcity.model.db;

import android.provider.BaseColumns;

/**
 * Created by RajeevTomar on 8/19/2015.
 */
public final  class FavouritePlacesContract {

    public FavouritePlacesContract()
    {

    }

    public static abstract class PlaceEntry implements BaseColumns
    {
        public static final String TABLE_NAME="favourite_place";
        public static final String COLUMN_NAME_PLACE_NAME = "place_name";
        public static final String COLUMN_NAME_PLACE_ID="place_id";
        public static final String COLUMN_NAME_PLACE_ADDRESS="place_address";
        public static final String COLUMN_NAME_PLACE_LAT="place_lat";
        public static final String COLUMN_NAME_PLACE_LONG="place_long";
        public static final String COLUMN_NAME_RATING="place_rating";
        public static final String COLUMN_NAME_VOTE="place_vote";
        public static final String COLUMN_NAME_CATEGORY="place_category";
        public static final String COLUMN_NAME_URL="place_url";
        public static final String COLUMN_NAME_DISTANCE="place_distance";
        public static final String COLUMN_NAME_VENUE_TYPE="venue_type";
    }
}
