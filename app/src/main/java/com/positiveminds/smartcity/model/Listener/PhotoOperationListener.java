package com.positiveminds.smartcity.model.Listener;

import com.positiveminds.smartcity.model.entity.*;

import java.util.List;

/**
 * Created by RajeevTomar on 8/26/2015.
 */
public interface PhotoOperationListener {

    public void onSuccessPhotosList(List<String> photoUrl);
    public void onFailurePhotosList(com.positiveminds.smartcity.model.entity.Error error);
}
