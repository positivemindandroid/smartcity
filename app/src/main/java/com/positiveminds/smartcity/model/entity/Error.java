package com.positiveminds.smartcity.model.entity;

/**
 * Created by RajeevTomar on 8/6/2015.
 */
public class Error {

    public static final int DEFAULT_CONNECTION_ERROR =  1000;
    public static final int NO_LOCATION_FOUND = 1001;
    public static final int NO_PLACE_FOUND =- 1002;
    public static final int EXCEPTION_ERROR = 1003;


    private int errorId;
    private String message;


    public Error()
    {

    }

    public Error(int errorId, String message)
    {
        this.errorId = errorId;
        this.message = message;
    }

    public int getErrorId() {
        return errorId;
    }

    public void setErrorId(int errorId) {
        this.errorId = errorId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
