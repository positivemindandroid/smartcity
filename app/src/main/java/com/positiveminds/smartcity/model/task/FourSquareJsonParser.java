package com.positiveminds.smartcity.model.task;

import android.app.job.JobScheduler;
import android.text.TextUtils;

import com.google.android.gms.maps.model.LatLng;
import com.positiveminds.smartcity.SmartCityApplication;
import com.positiveminds.smartcity.model.Listener.PhotoOperationListener;
import com.positiveminds.smartcity.model.Listener.PlaceDetailsOperationListener;
import com.positiveminds.smartcity.model.Listener.PlaceSearchOperationListener;
import com.positiveminds.smartcity.model.Listener.SuggestionOperationListener;
import com.positiveminds.smartcity.model.Utils;
import com.positiveminds.smartcity.model.entity.AutoSuggestion;
import com.positiveminds.smartcity.model.entity.Error;
import com.positiveminds.smartcity.model.entity.PlaceDetailsInfo;
import com.positiveminds.smartcity.model.entity.PlaceSearchInfo;
import com.positiveminds.smartcity.model.entity.UserReview;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RajeevTomar on 8/26/2015.
 */
public class FourSquareJsonParser {

    public static void parseVenueSearchJson(JSONObject response, PlaceSearchOperationListener listener) {
        Error error = new Error();
        try {
            if (response != null) {
                JSONObject metaJson = response.getJSONObject("meta");
                if (metaJson != null) {
                    int code = metaJson.getInt("code");
                    if (code != 200)
                        error.setErrorId(code);
                }
                JSONObject responseJson = response.getJSONObject("response");
                if (responseJson != null) {
                    JSONArray venuesJsonArr = responseJson.getJSONArray("venues");
                    if (venuesJsonArr != null && venuesJsonArr.length() > 0) {
                        List<PlaceSearchInfo> placeSearchInfoList = new ArrayList<>();
                        for (int i = 0; i < venuesJsonArr.length(); i++) {
                            JSONObject venueJson = venuesJsonArr.getJSONObject(i);
                            if (venueJson != null) {
                                PlaceSearchInfo searchInfo = new PlaceSearchInfo();
                                String id = venueJson.getString("id");
                                searchInfo.setPlaceId(id);
                                String name = venueJson.getString("name");
                                searchInfo.setName(name);
                                JSONObject contactJson = venueJson.getJSONObject("contact");
                                if (contactJson != null) {
                                    String phone = contactJson.has("phone") ? contactJson.getString("phone") : null;
                                    if (phone != null)
                                        searchInfo.setContactNumber(phone);
                                }
                                JSONObject locationJson = venueJson.getJSONObject("location");
                                if (locationJson != null) {
                                    String address = locationJson.has("address") ? locationJson.getString("address") : null;
                                    if (address != null)
                                        searchInfo.setAddress(address);
                                    double lat = locationJson.getDouble("lat");
                                    double lng = locationJson.getDouble("lng");
                                    LatLng latLng = new LatLng(lat, lng);
                                    searchInfo.setLocation(latLng);
                                    int distance = locationJson.has("distance") ? locationJson.getInt("distance") : -1;
                                    if (distance > 0)
                                        searchInfo.setDistance(distance / 1000);
                                    String cityName = locationJson.has("city") ? locationJson.getString("city") : null;
                                    if (cityName != null)
                                        searchInfo.setCityName(cityName);

                                }
                                String url = venueJson.has("url") ? venueJson.getString("url") : null;
                                if (url != null)
                                    searchInfo.setUrl(url);
                                placeSearchInfoList.add(searchInfo);
                            }
                        }
                        if (placeSearchInfoList.size() > 0)
                            listener.onSuccessPlaceSearch(placeSearchInfoList);
                    } else {
                        error.setErrorId(Error.NO_PLACE_FOUND);
                        error.setMessage("No Place Found");
                        listener.onFailurePlaceSearch(error);
                    }
                }
            }
        } catch (JSONException exception) {
            error.setErrorId(Error.EXCEPTION_ERROR);
            if (exception != null)
                error.setMessage(exception.getMessage());
            listener.onFailurePlaceSearch(error);
        }

    }

    public static void parseVenueExploreJson(JSONObject response, PlaceSearchOperationListener listener) {
        Error error = new Error();
        try {
            if (response != null) {
                JSONObject metaJson = response.getJSONObject("meta");
                if (metaJson != null) {
                    int code = metaJson.getInt("code");
                    if (code != 200)
                        error.setErrorId(code);
                }
                JSONObject responseJson = response.getJSONObject("response");
                if (responseJson != null) {
                    JSONArray groupJsonArr = responseJson.getJSONArray("groups");
                    if (groupJsonArr != null && groupJsonArr.length() > 0) {
                        for (int i = 0; i < groupJsonArr.length(); i++) {
                            JSONObject groupJson = groupJsonArr.getJSONObject(i);
                            if (groupJson != null) {
                                JSONArray itemJsonArr = groupJson.getJSONArray("items");
                                if (itemJsonArr != null && itemJsonArr.length() > 0) {
                                    int distanceType = SmartCityApplication.getAppInstance().getDistanceType();
                                    List<PlaceSearchInfo> placeSearchInfoList = new ArrayList<>();
                                    for (int j = 0; j < itemJsonArr.length(); j++) {
                                        JSONObject itemJson = itemJsonArr.getJSONObject(j);
                                        if (itemJson != null) {
                                            JSONObject venueJson = itemJson.getJSONObject("venue");

                                            if (venueJson != null) {
                                                PlaceSearchInfo searchInfo = new PlaceSearchInfo();
                                                String id = venueJson.getString("id");
                                                searchInfo.setPlaceId(id);
                                                int vote = venueJson.has("ratingSignals") ? venueJson.getInt("ratingSignals") : -1;
                                                searchInfo.setVote(vote);
                                                float ratingInt = venueJson.has("rating") ? venueJson.getLong("rating") : -1;
                                                searchInfo.setRatingInt(ratingInt);

                                                String name = venueJson.getString("name");
                                                searchInfo.setName(name);
                                                JSONObject contactJson = venueJson.getJSONObject("contact");
                                                if (contactJson != null) {
                                                    String phone = contactJson.has("phone") ? contactJson.getString("phone") : null;
                                                    if (phone != null)
                                                        searchInfo.setContactNumber(phone);
                                                }
                                                JSONObject locationJson = venueJson.getJSONObject("location");
                                                if (locationJson != null) {
                                                    String address = locationJson.has("address") ? locationJson.getString("address") : null;
                                                    if (address != null)
                                                        searchInfo.setAddress(address);
                                                    double lat = locationJson.getDouble("lat");
                                                    double lng = locationJson.getDouble("lng");
                                                    LatLng latLng = new LatLng(lat, lng);
                                                    searchInfo.setLocation(latLng);
                                                    int distance = locationJson.has("distance") ? locationJson.getInt("distance") : -1;
                                                    if (distance > 0) {
                                                        double distanceValue = Utils.meterToKMI(distance, distanceType);
                                                        searchInfo.setDistance(distanceValue);
                                                    }
                                                    String cityName = locationJson.has("city") ? locationJson.getString("city") : null;
                                                    if (cityName != null)
                                                        searchInfo.setCityName(cityName);

                                                }
                                                JSONObject photoJson = venueJson.getJSONObject("photos");
                                                if (photoJson != null) {
                                                    String imageUrl = getPhotoUrl(photoJson);
                                                    if (imageUrl != null)
                                                        searchInfo.setIconUrl(imageUrl);
                                                }
                                                JSONArray categoryJsonArr = venueJson.getJSONArray("categories");
                                                if (categoryJsonArr != null && categoryJsonArr.length() > 0) {
                                                    JSONObject categoryItemJson = categoryJsonArr.getJSONObject(0);
                                                    if (categoryItemJson != null) {
                                                        String category = categoryItemJson.getString("name");
                                                        searchInfo.setCategory(category);
                                                    }
                                                }
                                                String url = venueJson.has("url") ? venueJson.getString("url") : null;
                                                if (url != null)
                                                    searchInfo.setUrl(url);
                                                placeSearchInfoList.add(searchInfo);
                                            }
                                        }
                                    }
                                    if (placeSearchInfoList.size() > 0)
                                        listener.onSuccessPlaceSearch(placeSearchInfoList);
                                } else {
                                    error.setMessage("No result found nearby");
                                    listener.onFailurePlaceSearch(error);
                                }
                            }
                        }
                    }
                }
            }
        } catch (JSONException exception) {
            error.setErrorId(Error.EXCEPTION_ERROR);
            if (exception != null)
                error.setMessage(exception.getMessage());
            listener.onFailurePlaceSearch(error);
        }
    }

    private static String getPhotoUrl(JSONObject response) {
        try {
            if (response != null) {
                JSONArray groupJsonArr = response.getJSONArray("groups");
                if (groupJsonArr != null && groupJsonArr.length() > 0) {
                    JSONObject groupItemJson = groupJsonArr.getJSONObject(0);
                    if (groupItemJson != null) {
                        JSONArray groupItemArr = groupItemJson.getJSONArray("items");
                        if (groupItemArr != null && groupItemArr.length() > 0) {
                            JSONObject itemJson = groupItemArr.getJSONObject(0);
                            if (itemJson != null) {
                                String prefix = itemJson.getString("prefix");
                                String suffix = itemJson.getString("suffix");
                                String imageUrl = String.format("%s320x320%s", prefix, suffix);
                                return imageUrl;
                            }
                        }
                    }
                }

            }
        } catch (JSONException exception) {
            return null;
        }
        return null;
    }

    public static void parseVenueDetailJson(JSONObject response, PlaceDetailsOperationListener listener) {
        Error error = new Error();

        try {

            if (response != null) {
                JSONObject metaJson = response.getJSONObject("meta");
                if (metaJson != null) {
                    int code = metaJson.getInt("code");
                    if (code != 200)
                        error.setErrorId(code);
                }
                JSONObject responseJson = response.getJSONObject("response");
                if (responseJson != null) {
                    JSONObject venueJson = responseJson.getJSONObject("venue");
                    if (venueJson != null) {
                        PlaceDetailsInfo detailsInfo = new PlaceDetailsInfo();
                        JSONObject locationJson = venueJson.getJSONObject("location");
                        if (locationJson != null) {
                            String address = locationJson.has("address") ? locationJson.getString("address") : null;
                            if (address != null)
                                detailsInfo.setAddress(address);
                            double lat = locationJson.getDouble("lat");
                            double lng = locationJson.getDouble("lng");
                            LatLng latLng = new LatLng(lat, lng);
                            detailsInfo.setLocation(latLng);
                            String cityName = locationJson.has("city") ? locationJson.getString("city") : null;
                            if (cityName != null)
                                detailsInfo.setCityName(cityName);

                        }
                        JSONObject bestPhotoJson = venueJson.has("bestPhoto") ? venueJson.getJSONObject("bestPhoto") : null;
                        if (bestPhotoJson != null) {
                            String prefix = bestPhotoJson.getString("prefix");
                            String suffix = bestPhotoJson.getString("suffix");
                            String imageUrl = String.format("%s320x320%s", prefix, suffix);
                            detailsInfo.setImageUrl(imageUrl);
                        }
                        JSONObject contactJson = venueJson.getJSONObject("contact");
                        if (contactJson != null) {
                            String phone = contactJson.has("phone") ? contactJson.getString("phone") : null;
                            if (phone != null)
                                detailsInfo.setPhoneNumber(phone);
                        }
                        JSONObject hoursJson = venueJson.has("hours") ? venueJson.getJSONObject("hours") : null;
                        if (hoursJson != null) {
                            String status = hoursJson.has("status") ? hoursJson.getString("status") : null;
                            if (status != null)
                                detailsInfo.setOpenStatus(status);
                        }
                        int vote = venueJson.has("ratingSignals") ? venueJson.getInt("ratingSignals") : -1;
                        detailsInfo.setVote(vote);
                        float ratingInt = venueJson.has("rating") ? venueJson.getLong("rating") : -1;
                        detailsInfo.setRating(ratingInt);
                        String url = venueJson.has("url") ? venueJson.getString("url") : null;
                        detailsInfo.setWebsite(url);
                        String name = venueJson.getString("name");
                        detailsInfo.setPlaceName(name);
                        String foursquareUrl = venueJson.has("canonicalUrl") ? venueJson.getString("canonicalUrl") : null;
                        if (!TextUtils.isEmpty(foursquareUrl))
                            detailsInfo.setFoursquareUrl(foursquareUrl);

                        JSONObject menuJson = venueJson.has("menu") ? venueJson.getJSONObject("menu") : null;
                        if (menuJson != null) {
                            String mapUrl = menuJson.has("mobileUrl") ? menuJson.getString("mobileUrl") : menuJson.getString("url");
                            detailsInfo.setMenuUrl(mapUrl);
                        }
                        JSONArray categoryJsonArr = venueJson.getJSONArray("categories");
                        if (categoryJsonArr != null && categoryJsonArr.length() > 0) {
                            JSONObject categoryItemJson = categoryJsonArr.getJSONObject(0);
                            if (categoryItemJson != null) {
                                String category = categoryItemJson.getString("name");
                                detailsInfo.setCategory(category);
                            }
                        }
                        JSONObject tipsJson = venueJson.getJSONObject("tips");
                        if (tipsJson != null) {
                            List<UserReview> reviewList = getUserReviews(tipsJson, error);
                            if (reviewList != null) {
                                detailsInfo.setReviewList(reviewList);
                            }
                        }
                        listener.onSuccessGetDetails(detailsInfo);
                    }
                }

            }
        } catch (JSONException exception) {
            error.setErrorId(Error.EXCEPTION_ERROR);
            if (exception != null)
                error.setMessage(exception.getMessage());
            listener.onFailureGetDetails(error);
        }

    }

    private static List<UserReview> getUserReviews(JSONObject response, Error error) {
        try {
            if (response != null) {
                JSONArray groupJsonArr = response.getJSONArray("groups");
                if (groupJsonArr != null && groupJsonArr.length() > 0) {
                    JSONObject groupItemJson = groupJsonArr.getJSONObject(0);
                    if (groupItemJson != null) {
                        int count = groupItemJson.getInt("count");
                        if (count > 0) {
                            JSONArray itemJsonArr = groupItemJson.getJSONArray("items");
                            int length = itemJsonArr.length();
                            if (itemJsonArr != null && length > 0) {
                                length = length > 4 ? 4 : length;
                                List<UserReview> userReviews = new ArrayList<>();
                                for (int i = 0; i < length; i++) {
                                    JSONObject itemJson = itemJsonArr.getJSONObject(i);
                                    if (itemJson != null) {
                                        UserReview userReview = new UserReview();
                                        String text = itemJson.getString("text");
                                        userReview.setReviewMsg(text);
                                        long createdAt = itemJson.getLong("createdAt");
                                        userReview.setDate(createdAt);
                                        JSONObject userJson = itemJson.getJSONObject("user");
                                        if (userJson != null) {
                                            String firstName = userJson.has("firstName") ? userJson.getString("firstName") : "";
                                            String lastName = userJson.has("lastName") ? userJson.getString("lastName") : "";
                                            String userName = String.format("%s %s", firstName, lastName);
                                            userReview.setUserName(userName);
                                            JSONObject photoJson = userJson.getJSONObject("photo");
                                            if (photoJson != null) {
                                                String prefix = photoJson.getString("prefix");
                                                String suffix = photoJson.getString("suffix");
                                                String imageUrl = String.format("%s50x50%s", prefix, suffix);
                                                userReview.setProfileUrl(imageUrl);
                                            }
                                        }
                                        userReviews.add(userReview);
                                    }
                                }
                                return userReviews;
                            }
                        }
                    }
                }
            }
        } catch (JSONException exception) {
            error.setErrorId(Error.EXCEPTION_ERROR);
            if (exception != null)
                error.setMessage(exception.getMessage());
        }
        return null;
    }


    public static void parseVenuePhotoJson(JSONObject response, PhotoOperationListener listener) {
        Error error = new Error();
        try {
            if (response != null) {
                JSONObject metaJson = response.getJSONObject("meta");
                if (metaJson != null) {
                    int code = metaJson.getInt("code");
                    if (code != 200)
                        error.setErrorId(code);
                }
                JSONObject responseJson = response.getJSONObject("response");
                if (responseJson != null) {
                    JSONObject photoJson = responseJson.getJSONObject("photos");
                    if (photoJson != null) {
                        JSONArray itemJsonArr = photoJson.getJSONArray("items");
                        if (itemJsonArr != null && itemJsonArr.length() > 0) {
                            List<String> urlList = new ArrayList<>();
                            for (int i = 0; i < itemJsonArr.length(); i++) {
                                JSONObject itemJson = itemJsonArr.getJSONObject(i);
                                if (itemJson != null) {
                                    String prefix = itemJson.getString("prefix");
                                    String suffix = itemJson.getString("suffix");
                                    String imageUrl = String.format("%soriginal%s", prefix, suffix);
                                    urlList.add(imageUrl);
                                }
                            }
                            listener.onSuccessPhotosList(urlList);
                        } else {
                            error.setMessage("No Photos Found");
                            listener.onFailurePhotosList(error);
                        }
                    }
                }
            }
        } catch (JSONException exception) {
            error.setErrorId(Error.EXCEPTION_ERROR);
            if (exception != null)
                error.setMessage(exception.getMessage());
            listener.onFailurePhotosList(error);
        }
    }

    public static void parseAutoSuggestionList(JSONObject response, SuggestionOperationListener listener)
    {
        Error error = new Error();
        try {
            if (response != null) {
                JSONObject metaJson = response.getJSONObject("meta");
                if (metaJson != null) {
                    int code = metaJson.getInt("code");
                    if (code != 200)
                        error.setErrorId(code);
                }
                JSONObject responseJson = response.getJSONObject("response");
                if (responseJson != null) {
                    JSONArray miniVenuesArr = responseJson.getJSONArray("minivenues");
                    if(miniVenuesArr != null && miniVenuesArr.length() > 0)
                    {
                        List<AutoSuggestion> suggestions = new ArrayList<>();
                        for(int i=0; i< miniVenuesArr.length();i++)
                        {
                            JSONObject itemObject = miniVenuesArr.getJSONObject(i);
                            if(itemObject != null)
                            {
                                AutoSuggestion suggestion = new AutoSuggestion();
                                String name = itemObject.getString("name");
                                suggestion.setName(name);
                                JSONObject locationJson = itemObject.getJSONObject("location");
                                if (locationJson != null) {
                                    String address = locationJson.has("address") ? locationJson.getString("address") : null;
                                    suggestion.setAddress(address);
                                }
                                suggestions.add(suggestion);
                            }
                        }
                        listener.onSuccessfullGetSuggestion(suggestions);
                    }
                    else
                    {
                        error.setMessage("No Suggestion Found");
                    }
                }
            }
        }
        catch (JSONException exception) {
            error.setErrorId(Error.EXCEPTION_ERROR);
            if (exception != null)
                error.setMessage(exception.getMessage());
            listener.onFailureGetSuggestion(error);
        }
    }
}
