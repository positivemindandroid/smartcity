package com.positiveminds.smartcity.model.Listener;

import com.positiveminds.smartcity.model.entity.*;

import java.util.List;

/**
 * Created by RajeevTomar on 7/24/2015.
 */
public interface PlaceSearchOperationListener {

    public void onSuccessPlaceSearch(List<PlaceSearchInfo> searchInfoList);
    public void onFailurePlaceSearch(com.positiveminds.smartcity.model.entity.Error error);
}
