package com.positiveminds.smartcity.model.entity;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by RajeevTomar on 8/5/2015.
 */
public class PlaceDetailsInfo {

    private  String placeName;
    private String address;
    private String phoneNumber;
    private double rating;
    private String website;
    private String mapUrl;
    private LatLng location;
    private String cityName;
    private String imageUrl;
    private int vote;
    private String menuUrl;
    private String category;
    private List<UserReview> reviewList;
    private String openStatus;
    public String foursquareUrl;

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getMapUrl() {
        return mapUrl;
    }

    public void setMapUrl(String mapUrl) {
        this.mapUrl = mapUrl;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getVote() {
        return vote;
    }

    public void setVote(int vote) {
        this.vote = vote;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<UserReview> getReviewList() {
        return reviewList;
    }

    public void setReviewList(List<UserReview> reviewList) {
        this.reviewList = reviewList;
    }


    public String getOpenStatus() {
        return openStatus;
    }

    public void setOpenStatus(String openStatus) {
        this.openStatus = openStatus;
    }

    public String getFoursquareUrl() {
        return foursquareUrl;
    }

    public void setFoursquareUrl(String foursquareUrl) {
        this.foursquareUrl = foursquareUrl;
    }
}
