package com.positiveminds.smartcity;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLng;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.positiveminds.smartcity.model.ApplicationSettings;
import com.positiveminds.smartcity.model.constant.AppConstant;
import com.positiveminds.smartcity.model.entity.PlaceDetailsInfo;
import com.positiveminds.smartcity.model.entity.PlaceSearchInfo;

import java.util.List;

/**
 * Created by rajeev on 7/24/2015.
 */
public class SmartCityApplication extends Application {

    private static SmartCityApplication mAppInstance;
    private RequestQueue mRequestQueue;
    private List<PlaceSearchInfo> searchInfoList;
    private LatLng currentLocation;
    private int radius;
    private Typeface primaryTypface;
    private int distanceType;
    private PlaceSearchInfo detailsInfo;

    @Override
    public void onCreate() {
        super.onCreate();
        mAppInstance = this;
        init();
    }

    private void init()
    {
        radius = ApplicationSettings.getPref(AppConstant.PREF_RADIUS, AppConstant.RADIUS);
        distanceType = ApplicationSettings.getPref(AppConstant.PREF_DISTANCE_TYPE, AppConstant.MILE_TYPE);
        primaryTypface = Typeface.createFromAsset(getAssets(), AppConstant.FONT_PATH);
        initImageLoader(getApplicationContext());
    }

    public static void initImageLoader(Context context) {
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app
        ImageLoader.getInstance().init(config.build());
    }

    public static SmartCityApplication getAppInstance()
    {
        if(mAppInstance == null)
            throw  new NullPointerException("App Instance is null");
        return mAppInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public List<PlaceSearchInfo> getSearchInfoList() {
        return searchInfoList;
    }

    public void setSearchInfoList(List<PlaceSearchInfo> searchInfoList) {
        this.searchInfoList = searchInfoList;
    }

    public LatLng getCurrentLocation() {
//        return  new LatLng(28.619570, 77.088104);
        return currentLocation;
    }

    public void setCurrentLocation(LatLng currentLocation) {
        this.currentLocation = currentLocation;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public Typeface getPrimaryTypeface() {
        return primaryTypface;
    }

    public int getDistanceType() {
        return distanceType;
    }

    public void setDistanceType(int distanceType) {
        this.distanceType = distanceType;
    }

    public PlaceSearchInfo getDetailsInfo() {
        return detailsInfo;
    }

    public void setDetailsInfo(PlaceSearchInfo detailsInfo) {
        this.detailsInfo = detailsInfo;
    }
}
