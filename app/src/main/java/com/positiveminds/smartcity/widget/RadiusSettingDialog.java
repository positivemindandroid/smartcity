package com.positiveminds.smartcity.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.SmartCityApplication;
import com.positiveminds.smartcity.model.ApplicationSettings;
import com.positiveminds.smartcity.model.constant.AppConstant;

/**
 * Created by RajeevTomar on 8/7/2015.
 */
public class RadiusSettingDialog extends Dialog implements View.OnClickListener {

    private View mDialogView;
    private AnimationSet mModalInAnim;
    private AnimationSet mModalOutAnim;
    private Animation mOverlayOutAnim;
    private boolean mCloseFromCancel;
    private HoloCircleSeekBar mSeekBar;
    private Button mConfirmButton;
    private OnRadiusDialogClickListener mRadiusDialogClickListener;
    private Typeface myTypeface;
    private int mDistanceType;


    public static interface OnRadiusDialogClickListener {
        public void onConfirmClick(RadiusSettingDialog dialog, int radius);
        public void onCancelClick(RadiusSettingDialog radiusSettingDialog);
    }

    public RadiusSettingDialog(Context context)
    {
        super(context, R.style.alert_dialog);
        mDistanceType = SmartCityApplication.getAppInstance().getDistanceType();
        myTypeface = SmartCityApplication.getAppInstance().getPrimaryTypeface();
        setCancelable(true);
        setCanceledOnTouchOutside(false);
        mModalInAnim = (AnimationSet) OptAnimationLoader.loadAnimation(getContext(), R.anim.modal_in);
        mModalOutAnim = (AnimationSet) OptAnimationLoader.loadAnimation(getContext(), R.anim.modal_out);
        mModalOutAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mDialogView.setVisibility(View.GONE);
                mDialogView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (mCloseFromCancel) {
                            RadiusSettingDialog.super.cancel();
                        } else {
                            RadiusSettingDialog.super.dismiss();
                        }
                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        // dialog overlay fade out
        mOverlayOutAnim = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                WindowManager.LayoutParams wlp = getWindow().getAttributes();
                wlp.alpha = 1 - interpolatedTime;
                getWindow().setAttributes(wlp);
            }
        };
        mOverlayOutAnim.setDuration(120);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.radius_setting_dialog);
        mDialogView = getWindow().getDecorView().findViewById(android.R.id.content);
        TextView contentTextView = (TextView)findViewById(R.id.tv_content_text);
        contentTextView.setTypeface(myTypeface);
        mSeekBar = (HoloCircleSeekBar)findViewById(R.id.seek_bar);
        //mSeekBar.setInitPosition(SmartCityApplication.getAppInstance().getRadius());
        mConfirmButton = (Button)findViewById(R.id.btn_confirm);
        mConfirmButton.setOnClickListener(this);
        findViewById(R.id.btn_cancel).setOnClickListener(this);
        RadioButton kmRadion = (RadioButton)findViewById(R.id.radio_km);
        kmRadion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                   mDistanceType = AppConstant.KILOMETER_TYPE;
                    mSeekBar.setDistanceType(mDistanceType);
                }

            }
        });
        RadioButton mileRadio = (RadioButton)findViewById(R.id.radio_mile);
        mileRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                   mDistanceType = AppConstant.MILE_TYPE;
                    mSeekBar.setDistanceType(mDistanceType);
                }
            }
        });
        if(mDistanceType == AppConstant.KILOMETER_TYPE)
            kmRadion.setChecked(true);
        else
            mileRadio.setChecked(true);
    }


    protected void onStart() {
        mDialogView.startAnimation(mModalInAnim);
    }

    /**
     * The real Dialog.cancel() will be invoked async-ly after the animation finishes.
     */
    @Override
    public void cancel() {
        dismissWithAnimation(true);
    }

    /**
     * The real Dialog.dismiss() will be invoked async-ly after the animation finishes.
     */
    public void dismissWithAnimation() {
        dismissWithAnimation(false);
    }

    private void dismissWithAnimation(boolean fromCancel) {
        mCloseFromCancel = fromCancel;
        mConfirmButton.startAnimation(mOverlayOutAnim);
        mDialogView.startAnimation(mModalOutAnim);
    }

    public void setRadiusDilaogBtnClickListener( OnRadiusDialogClickListener clickListener )
    {
        this.mRadiusDialogClickListener = clickListener;
    }


    @Override
    public void onClick(View v) {
            switch (v.getId())
            {
                case R.id.btn_cancel:
                    if( mRadiusDialogClickListener != null)
                        mRadiusDialogClickListener.onCancelClick(RadiusSettingDialog.this);
                    break;
                case R.id.btn_confirm:
                    if( mSeekBar != null && mRadiusDialogClickListener != null)
                    {
                        int changedRadius = mSeekBar.getValue();
                        if(changedRadius == 0)
                            changedRadius = 3;
                        ApplicationSettings.putPref(AppConstant.PREF_DISTANCE_TYPE, mDistanceType);
                        SmartCityApplication.getAppInstance().setDistanceType(mDistanceType);
                        mRadiusDialogClickListener.onConfirmClick(RadiusSettingDialog.this, changedRadius);
                    }
                    break;
            }
    }


}
